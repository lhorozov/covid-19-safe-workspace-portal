# COVID-19-Safe Workspace Portal
Final Project Assignment
<br> by Lyuben Horozov and Radoslav Skenderov
<br>
<br>

## 1. Project Requirements

```bash
$ npm i

# front-end
$ npm start

# back-end
$ npm run start:dev
```
<br>
<br>

## 2. Database Configuration
Configure app.module.ts file: 
`back-end/src/app.module.ts`
and follow the next steps

```js
@Module({
  imports: [
    ControllersModule,
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '1234',
      database: 'covid',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
  ],
})
export class AppModule {}
```
You have to replace: <br>
* root with your database username _ _ _ <br>
* 1234 with your database password _ _ _ <br>
* covid with your database name _ _ _ <br>
<br>

Open MySQL Workbench and create new schema named `covid` or whatever name you prefer <br>
Stop back-end server with Ctrl+C and restore it with:
```bash
# back-end
$ npm run start:dev
```
to create all entities in the database. 
<br>
<br>
Open `covid-database.sql` file and execute it
<br>
<br>
<br>

## 3. Testing
There are two type of users: <br>
-Basic: email: `estewart@yahoo.com` , password: `user12` <br>
-Admin: email: `igeorgiev@yahoo.com` , password: `user12`
<br>
<br>
<br>
## 4. Structure
<br>

### 4.1 Public page
![Screenshot](readme-images/public.PNG)
* Home - viewing all countries floor schema - show users with his project assigned
* About us - information about developers as linkedin profiles
* Login - use credentails to initialize registered user
* Register - register basic user to the database
<br>
<br>

### 4.2 Private page

#### 4.2.1 User page
![Screenshot](readme-images/private.PNG)
* Home - viewing all countries floor schema - show users with his project assigned
* User
  - Schedule - check current country schedule and treshold, 
  - Vacations - check, add and remove user's vacations
  - Covid info - check average number of infected people for the last 7 days

* Logout - forward to the public part of the page
<br>
<br>

#### 4.2.1 Admin page
![Screenshot](readme-images/admin.PNG)

* Home - viewing all countries floor schema - show users with his project assigned
* Admin
  - Create country - Select country, Select number of desks, Select number of desks per row (should be number between 2 and 10), Select space between columns, Select Treshold down (by default is 5), Select Treshold up (by default is 10), Select distance between columns(by default is 3m), Select distance between columns(by default is 3m)
  
  <br>

  - Create project - Select country, Enter project's name, Enter project's description, Info table with current projects in the country

  <br>

  - Assign user - Assign user to project, Delete user to project, Info table with current users in the project

  <br>

  - Countries - Info table with all countries data: (Country Name,	Number of desks,	Number of desks per row,	Number of Desks in pair,	Treshold % between,	Distance row,	Distance column) and Change tresholds functionality
  
  <br>

  - Infected people - gather infected people about chosen country on demand and show that information in table, delete functionality
  

  - Desks report - current information about desks status (Country name,	Number of desks,	Occupied desks,	Forbidden desks,	Available desks)

* Logout - forward to the public part of the page
<br>
<br>
<br>

## 5. Business logic about schedule
### 5.1 Automatically set schedule for next week every friday at 19:30h (CronJob, server must be on)
### 5.2 Automatically get api data everyday at 11:30h (CronJob, server must be on)
### 5.3 General workspace rules and assumptions
* Check the ratio case about last 7 days, making desk plan about this ratio - if covid restriction set forbidden desks
* Get all users with project assigned and no vacation set for the entire week (vacation set in this week doesn't change this schedule, vacation set for one day = vacation for the entire week)
* As a first priority check if user was previous week in the office
* As a second priority sort all users by project
* Next week is dynamically generated based on last 7 days ratio
* Changing treshold values made on this week will be active on next week schedule

