import React, { useState } from 'react';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import Home from './components/Base/Home';

import AuthContext, { extractUser, getToken } from './providers/AuthContext';
import NotFound from './components/Base/NotFound';
import Admin from './components/Page/Admin/Admin';
import User from './components/Page/User/User';
import CreateCountry from './components/Page/Admin/CreateCountry';
import NavBar from './components/Base/NavBar';
import Countries from './components/Page/Admin/Countries';
import CreateProject from './components/Page/Admin/CreateProject';
import InfectedPeople from './components/Page/Admin/InfectedPeople';
import Projects from './components/Page/Admin/Projects';
import AboutUs from './components/Base/AboutUs';
import Vacation from './components/Page/User/Vacation'
import Schedule from './components/Page/User/Schedule'
import Register from './components/Page/Login/Register';
import Login from './components/Page/Login/Login';
import GuardedRoute from './providers/GuardedRoute';
import AdminGuardedRoute from './providers/AdminGuardedRoute';
import Reports from './components/Page/Admin/Reports';
import WeeklyCases from './components/Page/User/WeeklyCases';

function App() {
  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken()),
  });

  return (
    <div>
      <div id="wrap">
        <BrowserRouter>
          <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
            <NavBar />

            <Switch>
              <Redirect from="/" exact to="home" />
              <Route path="/home" component={Home} />
              <Route path="/register" component={Register} />
              <Route path="/login" component={Login} />
              <Route path="/about" component={AboutUs} />

              <GuardedRoute path="/user/vacation" auth={authValue.isLoggedIn} component={Vacation} />
              <GuardedRoute path="/user/schedule" auth={authValue.isLoggedIn} component={Schedule} />
              <GuardedRoute path="/user/weeklycases" auth={authValue.isLoggedIn} component={WeeklyCases} />
              <GuardedRoute path="/user" auth={authValue.isLoggedIn} component={User} />

              <AdminGuardedRoute path="/admin/createcountry" user={authValue.user} auth={authValue.isLoggedIn} component={CreateCountry} />
              <AdminGuardedRoute path="/admin/countries" user={authValue.user} auth={authValue.isLoggedIn} component={Countries} />
              <AdminGuardedRoute path="/admin/createproject" user={authValue.user} auth={authValue.isLoggedIn} component={CreateProject} />
              <AdminGuardedRoute path="/admin/projects" user={authValue.user} auth={authValue.isLoggedIn} component={Projects} />
              <AdminGuardedRoute path="/admin/infectedpeople" user={authValue.user} auth={authValue.isLoggedIn} component={InfectedPeople} />
              <AdminGuardedRoute path="/admin/reports" user={authValue.user} auth={authValue.isLoggedIn} component={Reports} />
              
              <Route path="*" component={NotFound} />

            </Switch>

          </AuthContext.Provider>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
