import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const toastSuccess = (message) => {
  return toast.success(`${message}`, {
    position: "top-right",
    autoClose: 1500,
    });
}

export const toastError = (message) => {
  return toast.error(`${message}`, {
    position: "top-right",
    autoClose: 1500,
    });
}