import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

const AdminGuardedRoute = ({ component: Component, auth, user, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        auth === true && user.role === 'Admin' ? (
          <Component {...props} />
        ) : (
          <Redirect to="/home" />
        )
      }
    />
  );
};

AdminGuardedRoute.propTypes = {
  component: PropTypes.func.isRequired,
  auth: PropTypes.bool.isRequired,
};

export default AdminGuardedRoute;
