import jwtDecode from 'jwt-decode';
import { createContext } from 'react';

const AuthContext = createContext({
  isLoggedIn: false,
  user: null,
  setLoginState: () => {},
});

export const getToken = () => localStorage.getItem('token') || '';

export const extractUser = (token) => {
  try {
    return jwtDecode(token) || null;
  } catch (e) {
    localStorage.removeItem('token');
    return null;  
  }
};

export default AuthContext;
