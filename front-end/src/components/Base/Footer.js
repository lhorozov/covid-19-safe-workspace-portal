import React from 'react'

const Footer = () => {
  return (
    <div id='footer'>
      <p>&copy; Telerik 2020 | Library Web project</p>
    </div>
  )
}

export default Footer
