import React, { useContext, useState } from "react";
import { Nav } from "react-bootstrap";
import AuthContext from "../../providers/AuthContext";
import { NavLink } from "react-router-dom";
import { MDBIcon } from "mdbreact";

const NavBar = () => {
  const { user, setLoginState } = useContext(AuthContext);
  const [startAnimation, setstartAnimation] = useState("animation start-home");
  const logout = () => {
    setLoginState({
      isLoggedIn: false,
      user: null,
    });
    localStorage.removeItem('token');
  };

  return (
    <div id="navdiv">
      <nav>
        <Nav.Link
          className="aHome"
          as={NavLink}
          exact
          to="/home"
          onClick={() => setstartAnimation("animation start-home")}
        >
          Home
        </Nav.Link>

        {user?.role === "Admin" ? (
          <>
            <Nav.Link
              className="aHome"
              as={NavLink}
              exact
              to="/admin/createcountry"
              onClick={() => setstartAnimation('animation start-about')}
            >
              Admin
            </Nav.Link>
          </>
        ) : null}
        {user?.role === 'Basic' ? (
          <>
            <Nav.Link
              className="aHome"
              as={NavLink}
              exact
              to="/user/schedule"
              onClick={() => setstartAnimation('animation start-about')}
            >
              User
            </Nav.Link>
          </>
        ) : null}

        {!user ? (
          <>
            <Nav.Link
              className="aHome"
              as={NavLink}
              exact
              to="/about"
              onClick={() => setstartAnimation("animation start-about")}
            >
              About us
            </Nav.Link>
            {/* <Nav.Link className="aHome" href='http://localhost:4000/about' onClick={()=>setstartAnimation('animation start-about')}>About us</Nav.Link> */}
          </>
        ) : null}
        {user ? (
          <>
            <Nav.Link className="aHome aHomeIcon">
              <MDBIcon icon="address-card" className="text-white" />
            </Nav.Link>
            <Nav.Link
              className="aHome"
              as={NavLink}
              exact
              to="/home"
              onClick={(() => setstartAnimation('animation start-blog'), logout)}
            >
              Logout
            </Nav.Link>
            <Nav.Link className="aHome aHomeEmail">{user.email}</Nav.Link>
          </>
        ) : (
          <>
            <Nav.Link
              className="aHome"
              as={NavLink}
              exact
              to="/login"
              onClick={() => setstartAnimation('animation start-blog')}
            >
              Login
            </Nav.Link>
            <Nav.Link
              className="aHome"
              as={NavLink}
              exact
              to="/register"
              onClick={() => setstartAnimation('animation start-portefolio')}
            >
              Register
            </Nav.Link>
          </>
        )}

        <div className={startAnimation}></div>
      </nav>
    </div>
  );
};

export default NavBar;
