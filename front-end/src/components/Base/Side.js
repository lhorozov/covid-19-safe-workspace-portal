import React, { useContext } from "react";
import { Nav } from "react-bootstrap";
import { withRouter, NavLink } from "react-router-dom";
import AuthContext from "../../providers/AuthContext";
import "./Side.css";
const Side = (props) => {
  const { user } = useContext(AuthContext);

  // const location = props.location;
  // const isLoginAdmin = location.pathname.includes('admin');
  // const isLoginUser = location.pathname.includes('user');

  return (
    <>
      <div className="col-2" id="sticky-sidebar">
        <div className="sticky-top">
          <ul className="ulSide">
            {user?.role === "Admin" ? (
              <>
                <li className="liSide">
                  <Nav.Link
                    as={NavLink}
                    className="aSide"
                    exact
                    to="/admin/createcountry"
                  >
                    Create Country
                  </Nav.Link>
                </li>
                <li className="liSide">
                  <Nav.Link
                    as={NavLink}
                    className="aSide"
                    exact
                    to="/admin/createproject"
                  >
                    Create Project
                  </Nav.Link>
                </li>
                <li className="liSide">
                  {" "}
                  <Nav.Link
                    as={NavLink}
                    className="aSide"
                    exact
                    to="/admin/projects"
                  >
                    Assign user
                  </Nav.Link>
                </li>
                <li className="liSide">
                  {" "}
                  <Nav.Link
                    as={NavLink}
                    className="aSide"
                    exact
                    to="/admin/countries"
                  >
                    Countries
                  </Nav.Link>
                </li>
                <li className="liSide">
                  {" "}
                  <Nav.Link
                    as={NavLink}
                    className="aSide"
                    exact
                    to="/admin/infectedpeople"
                  >
                    Infected people
                  </Nav.Link>
                </li>
                <li className="liSide">
                  {" "}
                  <Nav.Link
                    as={NavLink}
                    className="aSide"
                    exact
                    to="/admin/reports"
                  >
                    Desks report
                  </Nav.Link>
                </li>
              </>
            ) : null}
            {user?.role === "Basic" ? (
              <>
                <li className="liSide">
                  {" "}
                  <Nav.Link
                    as={NavLink}
                    className="aSide"
                    exact
                    to="/user/schedule"
                  >
                    Schedule
                  </Nav.Link>
                </li>
                <li className="liSide">
                  {" "}
                  <Nav.Link
                    as={NavLink}
                    className="aSide"
                    exact
                    to="/user/vacation"
                  >
                    Vacations
                  </Nav.Link>
                </li>
                <li className="liSide">
                  {" "}
                  <Nav.Link
                    as={NavLink}
                    className="aSide"
                    exact
                    to="/user/weeklycases"
                  >
                    Covid Info
                  </Nav.Link>
                </li>
              </>
            ) : null}
          </ul>
        </div>
      </div>
    </>
  );
};

export default withRouter(Side);
