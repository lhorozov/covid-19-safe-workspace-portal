import React from 'react';
import { CardDeck, CardGroup } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import './AboutUs.css';
import LogoLyuben from './images/lyuben.PNG';
import LogoRadi from './images/radi.PNG';

const AboutUs = () => {
  return (
    <div className='container'>
      <br></br>
      <CardDeck>
        <Card>
          <div>
            <a className="LI-simple-link" href="https://bg.linkedin.com/in/lyubenhorozov?trk=profile-badge">
              <img src={LogoLyuben} />
            </a>
          </div>
          {/* <div class="LI-profile-badge"  data-version="v1" data-size="large" data-locale="en_US" data-type="vertical" data-theme="dark" data-vanity="lyubenhorozov"><a class="LI-simple-link" href='https://bg.linkedin.com/in/lyubenhorozov?trk=profile-badge'>Lyuben Horozov</a></div> */}
        </Card>
        <Card>
          <div>
            <a className="LI-simple-link" href="https://bg.linkedin.com/in/radoslav-skenderov-61b48719b?trk=profile-badge">
              <img src={LogoRadi} />
            </a>
          </div>
        </Card>
      </CardDeck>
    </div>
  );
};

export default AboutUs;
