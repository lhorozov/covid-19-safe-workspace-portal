import React, { useContext, useEffect, useState } from "react";
import { Dropdown, DropdownButton, Button } from "react-bootstrap";
import AuthContext, { getToken } from "../../../providers/AuthContext";
import { BASE_URL } from "../../../common/constants";
import Side from "./../../Base/Side";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import "react-toastify/dist/ReactToastify.css";
import Loading from "./../../Base/Loading";

const WeeklyCases = () => {
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [dropValue, setDropValue] = useState("Select country");
  const [avgCases, setAvgCases] = useState(null);
  const [countriesDropMenu, setCountriesDropMenu] = useState([]);

  useEffect(() => {
    setLoading(true);
    fetch(`https://coronavirus-19-api.herokuapp.com/countries`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setCountriesDropMenu(result.map((c) => c.country));
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const getInfectedPeople = (ctr) => {
    setLoading(true);
    fetch(`${BASE_URL}/country/${ctr}/weektotalcases`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setAvgCases(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  };

  const flag = (f) => {
    return `${f.toLowerCase()} flag`;
  };

  if (error) {
    return <h1>{error}</h1>;
  }

  if (loading) {
    return <Loading />;
  }

  return (
    <div className="container-fluid">
    <div className="row py-3">
      <Side />
      <div class="col" id="main">
      <h1 className="my-5">Average cases for the last 7 days</h1>

          <DropdownButton
            onSelect={(e) => (getInfectedPeople(e), setDropValue(e))}
            className="drop"
            id="dropdown-basic-button"
            title={dropValue}
          >
            {countriesDropMenu
              .sort((a, b) => a.localeCompare(b))
              .map((r, i) => (
                <Dropdown.Item key={i} eventKey={r}>
                  <i className={flag(r)}></i>
                  {r}
                </Dropdown.Item>
              ))}
          </DropdownButton>

          <MDBTable>
            <MDBTableHead color="blue-gradient" textWhite>
              <tr>
                <th>#</th>
                <th>Country</th>
                <th>Cases</th>
                <th>Today cases</th>
                <th>Deaths</th>
                <th>Today Deaths</th>
                <th>Recovered</th>
                <th>Active</th>
                <th>Critical</th>
              </tr>
            </MDBTableHead>

            <MDBTableBody>
              {avgCases !== null ? (
                <tr key={0}>
                  <td>{1}</td>
                  <td>
                    <i className={flag(avgCases.country)}></i>{" "}
                    {avgCases.country}
                  </td>
                  <td>{Math.round(avgCases.cases)}</td>
                  <td>{Math.round(avgCases.todayCases)}</td>
                  <td>{Math.round(avgCases.deaths)}</td>
                  <td>{Math.round(avgCases.todayDeaths)}</td>
                  <td>{Math.round(avgCases.recovered)}</td>
                  <td>{Math.round(avgCases.active)}</td>
                  <td>{Math.round(avgCases.critical)}</td>
                </tr>
              ) : null}
            </MDBTableBody>
          </MDBTable>
        </div>
        <div className="col-1"></div>
      </div>
    </div>
  );
};

export default WeeklyCases;
