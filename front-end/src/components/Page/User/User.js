import React from "react";
import Side from "../../Base/Side";

const User = () => {
  return (
    <div className="container">
      <Side />
      <h1>User page</h1>
    </div>
  );
};

export default User;
