import React, { useContext, useState, useEffect } from 'react';
import AuthContext, { getToken } from '../../../providers/AuthContext';
import { Table } from 'react-bootstrap';
import { BASE_URL } from '../../../common/constants';
import './Schedule.css';
import { MDBIcon } from 'mdbreact';
import Side from '../../Base/Side';
import Loading from '../../Base/Loading';

const Schedule = () => {
  const { user } = useContext(AuthContext);

  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [matrix, setMatrix] = useState([]);
  const [matrixNextweek, setMatrixNextweek] = useState([]);
  const [columnSpace, setColumnSpace] = useState(2);
  const [distanceX, setDistanceX] = useState(3);
  const [distanceY, setDistanceY] = useState(2);
  const [downTreshold, setDownTreshold] = useState('');
  const [upTreshold, setUpTreshold] = useState('');
  const [text, setText] = useState(false);

  useEffect(() => {
    fetch(`${BASE_URL}/country/${user.country}/schedule`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setMatrix(result);
        return fetch(`${BASE_URL}/country/${user.country}/nextschedule`)
          .then((response) => response.json())
          .then((result2) => {
            setMatrixNextweek(result2);

            return fetch(`${BASE_URL}/country/`)
              .then((response) => response.json())
              .then((result3) => {
                setDistanceX(result3.find((c) => c.name === user.country).setDistanceX);
                setDistanceY(result3.find((c) => c.name === user.country).setDistanceY);
                setColumnSpace(result3.find((c) => c.name === user.country).columnSpace);
                setDownTreshold(result3.find((c) => c.name === user.country).downTresholdValue);
                setUpTreshold(result3.find((c) => c.name === user.country).upTresholdValue);
                setText(true);
              });
          });
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const colFunc = (col, j) => {
    if (col === '2') {
      return (
        <td key={j} className="forbidden text-center">
          <MDBIcon icon="times-circle" className="xmark" />
        </td>
      );
    } else if (col === '1') {
      return (
        <td key={j} className="freedesk text-center">
          Free
        </td>
      );
    } else if (col === '0') {
      return <td key={j} className="nodesk text-center"></td>;
    } else {
      return (
        <td key={j} className="col-matrix text-center">
          {col}
        </td>
      );
    }
  };

  const flag = (f) => {
    return `${f.toLowerCase()} flag`;
  };

  if (error) {
    return <h1>{error}</h1>;
  }

  if (loading) {
    return <Loading />;
  }

  return (
    <div className="container-fluid">
      <div className="row py-3">
        <Side />
        <div className="col" id="main">
          <h3 className="mt-5">
            Hello, <b className="font-weight-bold">{user.username}</b>! <br />
            Please check your schedule for <b className="font-weight-bold">{user.country}</b> <br />
            {user.project ? (
              <>
              Your project is:   <b className="font-weight-bold">{user.project}</b> <br />
              </>
            ) : (
              <>
                You don't have any project <br></br>
              </>
            )}
            Current threshold values are <b className="font-weight-bold">{downTreshold}%</b> and{' '}
            <b className="font-weight-bold">{upTreshold}%</b>
          </h3>
          {text ? (
            <>
              <hr className="style-eight firstHr"></hr>
              <h1 className="week-title">This week</h1>
              <hr className="style-eight"></hr>
            </>
          ) : null}
          <Table responsive className="borderless">
            <tbody>
              {matrix.length > 0
                ? matrix.map((row, i) => {
                    return i % 2 === 0 && i !== 0 ? (
                      <>
                        <tr key={i} className="empty-row ">
                          <td key={i + 1000} className="align-middle">
                            {' '}
                            <h4>{distanceY}m </h4>{' '}
                            <svg
                              key={i}
                              width="1.5em"
                              height="1.5em"
                              viewBox="0 0 16 16"
                              className="bi bi-arrow-down-up"
                              fill="currentColor"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                fillRule="evenodd"
                                d="M11.5 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L11 2.707V14.5a.5.5 0 0 0 .5.5zm-7-14a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L4 13.293V1.5a.5.5 0 0 1 .5-.5z"
                              />
                            </svg>
                          </td>
                        </tr>
                        <tr key={i + 100} className="row-matrix">
                          {row.map((col, j) => {
                            return (j + 1) % columnSpace === 0 ? (
                              <>
                                {colFunc(col, j)}
                                {i === 0 ? (
                                  <td key={j + 100} className="text-center empty-col">
                                    {distanceX}m
                                  </td>
                                ) : (
                                  <td key={j + 100} className="empty-col"></td>
                                )}
                              </>
                            ) : (
                              colFunc(col, j)
                            );
                          })}
                        </tr>
                      </>
                    ) : (
                      <tr key={i + 100} className="row-matrix">
                        {row.map((col, j) => {
                          return (j + 1) % columnSpace === 0 ? (
                            <>
                              {colFunc(col, j)}
                              {i === 0 && row.length - 1 !== j ? (
                                <td key={j + 100} className="empty-col text-center">
                                  <span> {distanceX}m </span>{' '}
                                  <svg
                                    key={j + 100}
                                    width="1.5em"
                                    height="1.5em"
                                    viewBox="0 0 16 16"
                                    className="bi bi-arrow-left-right"
                                    fill="currentColor"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      fillRule="evenodd"
                                      d="M1 11.5a.5.5 0 0 0 .5.5h11.793l-3.147 3.146a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 11H1.5a.5.5 0 0 0-.5.5zm14-7a.5.5 0 0 1-.5.5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H14.5a.5.5 0 0 1 .5.5z"
                                    />
                                  </svg>
                                </td>
                              ) : (
                                <td key={j + 100} className="empty-col"></td>
                              )}
                            </>
                          ) : (
                            colFunc(col, j)
                          );
                        })}
                      </tr>
                    );
                  })
                : null}
            </tbody>
          </Table>
          {text ? (
            <>
              <hr className="style-eight"></hr>
              <h1 className="week-title">Next week</h1>
              <hr className="style-eight"></hr>{' '}
            </>
          ) : null}

          <Table responsive className="borderless">
            <tbody>
              {matrixNextweek.length > 0
                ? matrixNextweek.map((row, i) => {
                    return i % 2 === 0 && i !== 0 ? (
                      <>
                        <tr key={i} className="empty-row ">
                          <td key={i + 111} className="align-middle">
                            <h4> {distanceX}m </h4>{' '}
                            <svg
                              key={i}
                              width="1.5em"
                              height="1.5em"
                              viewBox="0 0 16 16"
                              className="bi bi-arrow-down-up"
                              fill="currentColor"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                fillRule="evenodd"
                                d="M11.5 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L11 2.707V14.5a.5.5 0 0 0 .5.5zm-7-14a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L4 13.293V1.5a.5.5 0 0 1 .5-.5z"
                              />
                            </svg>
                          </td>
                        </tr>
                        <tr key={i + 100} className="row-matrix">
                          {row.map((col, j) => {
                            return (j + 1) % columnSpace === 0 ? (
                              <>
                                {colFunc(col, j)}
                                {i === 0 ? (
                                  <td key={j + 100} className="text-center empty-col">
                                    {distanceY}m
                                  </td>
                                ) : (
                                  <td key={j + 100} className="empty-col"></td>
                                )}
                              </>
                            ) : (
                              colFunc(col, j)
                            );
                          })}
                        </tr>
                      </>
                    ) : (
                      <tr key={i + 100} className="row-matrix">
                        {row.map((col, j) => {
                          return (j + 1) % columnSpace === 0 ? (
                            <>
                              {colFunc(col, j)}
                              {i === 0 && row.length - 1 !== j ? (
                                <td key={j + 100} className="empty-col text-center">
                                  <span> {distanceX}m </span>{' '}
                                  <svg
                                    key={j + 100}
                                    width="1.5em"
                                    height="1.5em"
                                    viewBox="0 0 16 16"
                                    className="bi bi-arrow-left-right"
                                    fill="currentColor"
                                    xmlns="http://www.w3.org/2000/svg"
                                  >
                                    <path
                                      fillRule="evenodd"
                                      d="M1 11.5a.5.5 0 0 0 .5.5h11.793l-3.147 3.146a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 11H1.5a.5.5 0 0 0-.5.5zm14-7a.5.5 0 0 1-.5.5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H14.5a.5.5 0 0 1 .5.5z"
                                    />
                                  </svg>
                                </td>
                              ) : (
                                <td key={j + 100} className="empty-col"></td>
                              )}
                            </>
                          ) : (
                            colFunc(col, j)
                          );
                        })}
                      </tr>
                    );
                  })
                : null}
            </tbody>
          </Table>
        </div>
        <div className="col-1"></div>
      </div>
    </div>
  );
};

export default Schedule;
