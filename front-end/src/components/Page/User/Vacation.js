import React, { useEffect, useState } from "react";
import { FormGroup, FormControl, FormLabel, Button } from "react-bootstrap";
import { getToken } from "../../../providers/AuthContext";
import { BASE_URL } from "../../../common/constants";
import Side from "./../../Base/Side";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import { ToastContainer } from "react-toastify";
import { toastError, toastSuccess } from "../../../common/utils";

const Vacation = () => {
  const [startDate, setStartDate] = useState(undefined);
  const [endDate, setEndDate] = useState(undefined);
  const [error, setError] = useState(false);
  const [vacation, setVacation] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/users/vacation`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setVacation(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const createVacation = () => {
    const startDateNumber = new Date(startDate).getTime();
    const endDateNumber = new Date(endDate).getTime();

    if (!startDateNumber || !endDateNumber) {
      return toastError("Please add start and end dates");
    }
    if (startDateNumber >= endDateNumber) {
      return toastError("Please add correct start and end dates");
    }

    fetch(`${BASE_URL}/users/vacation`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify({
        startDate: startDate,
        endDate: endDate,
      }),
    })
      .then((r) => r.json())
      .then((result) => {
        setVacation(result);
        setStartDate("");
        setEndDate("");
        toastSuccess("Vacation added!");
      })
      .catch((error) => setError(error.message));
  };

  const deleteVacation = (id) => {
    fetch(`${BASE_URL}/users/${id}/vacation/`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then(() => {
        setVacation(vacation.filter((v) => v.id !== id));
        toastSuccess("Vacation deleted!");
      })
      .catch((err) => setError(err.message));
  };

  if (error) {
    return <h1>{error}</h1>;
  }

  return (
    <div className="container-fluid">
      <div className="row py-3">
        <ToastContainer />
        <Side />
        <div class="col" id="main">
          <h1 className="my-4">Vacation page</h1>
          <FormGroup controlId="date" bssize="large">
            <FormLabel>Start Vacation</FormLabel>
            <FormControl
              type="date"
              onChange={(e) => setStartDate(e.target.value)}
            />
          </FormGroup>
          <FormGroup controlId="date" bssize="large">
            <FormLabel>End Vacation</FormLabel>
            <FormControl
              type="date"
              onChange={(e) => setEndDate(e.target.value)}
            />
          </FormGroup>
          <Button className="my-3 mb-5 add" onClick={createVacation}>
            Add vacation
          </Button>

          <MDBTable>
            <MDBTableHead color="blue-gradient" textWhite>
              <tr>
                <th>#</th>
                <th>Start Date Vacation</th>
                <th>End Date Vacation</th>
                <th>Delete Vacation</th>
              </tr>
            </MDBTableHead>
            {
              <MDBTableBody>
                {vacation.length > 0
                  ? vacation.map((v, i) => (
                      <tr key={i}>
                        <td>{i + 1}</td>
                        <td>{v.startDate}</td>
                        <td>{v.endDate}</td>
                        <td>
                          <Button
                            className="delete"
                            onClick={() => deleteVacation(v.id)}
                          >
                            Delete
                          </Button>
                        </td>
                      </tr>
                    ))
                  : null}
              </MDBTableBody>
            }
          </MDBTable>
        </div>
        <div className="col-1"></div>
      </div>
    </div>
  );
};

export default Vacation;
