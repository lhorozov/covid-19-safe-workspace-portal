import React, { useEffect, useState } from "react";
import { getToken } from "../../../providers/AuthContext";
import Side from "../../Base/Side";
import { BASE_URL } from "../../../common/constants";
import {
  Button,
  Col,
  Dropdown,
  DropdownButton,
  Form,
  Row,
} from "react-bootstrap";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import Loading from "../../Base/Loading";
import { ToastContainer } from "react-toastify";
import { toastSuccess, toastError } from "../../../common/utils";

const CreateProject = () => {
  const [countries, setcountries] = useState([]);
  const [projects, setProjects] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [dropValue, setDropValue] = useState("Select country");
  const [projectName, setprojectName] = useState("");
  const [projectDescription, setprojectDescription] = useState("");

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/country`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setcountries(result.map((r) => r.name));
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const getCountryProject = (country) => {
    setLoading(true);
    fetch(`${BASE_URL}/projects/${country}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setProjects(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  };

  const createCountryProject = (project) => {
    if (dropValue === "Select country") {
      return toastError("Please Select a country");
    }
    if (!projectName) {
      return toastError("Please add name for this project");
    }
    if (!projectDescription) {
      return toastError("Please add description for this project");
    }

    fetch(`${BASE_URL}/projects/${project}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify({
        name: projectName,
        description: projectDescription,
      }),
    })
      .then((r) => r.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setProjects([...projects, result]);
        toastSuccess("Project added successful");
        setprojectName("");
        setprojectDescription("");
        setDropValue("Select country");
      })
      .catch((error) => toastError(error.message));
  };

  const flag = (f) => {
    return `${f.toLowerCase()} flag`;
  };

  if (loading) {
    return <Loading />;
  }

  return (
    <div className="container-fluid">
      <div className="row py-3">
        <ToastContainer />
        <Side />
        <div class="col" id="main">
          <h1 className="my-5"> Create Project</h1>
          <DropdownButton
            onSelect={(e) => (setDropValue(e), getCountryProject(e))}
            className="drop"
            id="dropdown-basic-button"
            title={dropValue}
          >
            {countries
              .sort((a, b) => a.localeCompare(b))
              .map((r, i) => (
                <Dropdown.Item key={i} eventKey={r}>
                  <i className={flag(r)}></i>
                  {r}
                </Dropdown.Item>
              ))}
          </DropdownButton>
          <Form>
            <Form.Group as={Row} controlId="formPlaintextPassword">
              <Form.Label column sm="2">
                Please enter project name
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="text"
                  placeholder="e.g. Covid project"
                  value={projectName}
                  onChange={(e) => setprojectName(e.target.value)}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} controlId="formPlaintextPassword">
              <Form.Label column sm="2">
                Please enter project description
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="textarea"
                  value={projectDescription}
                  placeholder="Please add description for this project"
                  onChange={(e) => setprojectDescription(e.target.value)}
                />
              </Col>
            </Form.Group>

            <Button
              className="add"
              onClick={() => createCountryProject(dropValue)}
            >
              Create project
            </Button>
            <br></br>
            <br></br>
            <h1>Current projects in this country:</h1>
            <br></br>
            {projects.length > 0 ? (
              <MDBTable>
                <MDBTableHead color="blue-gradient" textWhite>
                  <tr>
                    <th>#</th>
                    <th>Project Name</th>
                    <th>Project descrpition</th>
                  </tr>
                </MDBTableHead>
                <MDBTableBody>
                  {projects.map((p, i) => (
                    <tr key={i}>
                      <td>{i + 1}</td>
                      <td>{p.name}</td>
                      <td>{p.description}</td>
                    </tr>
                  ))}
                </MDBTableBody>
              </MDBTable>
            ) : (
              <h3>No projects to show</h3>
            )}
          </Form>
        </div>
      </div>
      <div className="col-1"></div>
    </div>
  );
};

export default CreateProject;
