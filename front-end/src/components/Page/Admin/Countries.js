import React, { useEffect, useState } from 'react';
import { getToken } from '../../../providers/AuthContext';
import { BASE_URL } from '../../../common/constants';
import Side from '../../Base/Side';
import { MDBIcon, MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import { ToastContainer } from 'react-toastify';
import { toastError, toastSuccess } from '../../../common/utils';

const Countries = () => {
  const [countries, setCountries] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [showApplyEditBtn, setShowApplyEditBtn] = useState('');
  const [downTresNew, setDownTresNew] = useState();
  const [upTresNew, setUpTresNew] = useState();

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/country`, {
      headers: {
        // Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setCountries(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const flag = (f) => {
    return `${f.toLowerCase()} flag`;
  };

  const changeTres = (country) => {
    if (
      +downTresNew < 0 ||
      +upTresNew < 0 ||
      +downTresNew > 100 ||
      +upTresNew > 100
    ) {
      return toastError('Please provide values between 0 and 100');
    }
    if (+downTresNew >= +upTresNew) {
      return toastError("Down treshold can't be greater then Up treshold");
    }
    fetch(`${BASE_URL}/country/${country}`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        downTreshold: downTresNew,
        upTreshold: upTresNew,
      }),
    })
      .then((res) => res.json())
      .then((result) => {
        setCountries(
          countries.map((c) => {
            if (c.id === result.id) {
              return result;
            }
            return c;
          })
        );
        setShowApplyEditBtn('');
        toastSuccess('Tresholds changed successfully');
      })
      .catch((err) => setError(err.message));
  };

  const getTotalInfectedPerCountry = (country) => {
    setLoading(true);
    fetch(`https://coronavirus-19-api.herokuapp.com/countries/${country}`)
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        console.log(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  };

  return (
    <div className='container-fluid'>
      <div className='row py-3'>
        <ToastContainer />
        <Side />
        <div class='col' id='main'>
          <h1 className='my-5'>All countries</h1>
          <MDBTable>
            <MDBTableHead color='blue-gradient' textWhite>
              <tr>
                <th>#</th>
                <th>Country Name</th>
                <th>Number of desks</th>
                <th>Number of desks per row</th>
                <th>Number of Desks in pair</th>
                <th>Treshold % between</th>
                <th>Distance row</th>
                <th>Distance column</th>
              </tr>
            </MDBTableHead>
            {
              <MDBTableBody>
                {countries
                  ? countries.map((c, i) => (
                      <tr key={i}>
                        <td>{i + 1}</td>
                        <td>
                          {' '}
                          <i className={flag(c.name)}></i>
                          {c.name}
                        </td>
                        <td>{c.numberOfDesks}</td>
                        <td>{c.numberOfDesksPerRow}</td>
                        <td>{c.columnSpace}</td>
                        <td>
                          {showApplyEditBtn === c.name ? (
                            <>
                              <input
                                type='number'
                                className='w-25'
                                placeholder={c.downTresholdValue}
                                onChange={(e) => setDownTresNew(e.target.value)}
                              ></input>
                              <input
                                type='number'
                                className='w-25'
                                placeholder={c.upTresholdValue}
                                onChange={(e) => setUpTresNew(e.target.value)}
                              ></input>
                              <button
                                className=' btn-floating btn-secondary btn-sm ml-3'
                                onClick={() => changeTres(c.name)}
                              >
                                <MDBIcon icon='edit' className='text-white ' />
                              </button>
                            </>
                          ) : (
                            <>
                              {c.downTresholdValue} to {c.upTresholdValue}
                              <button
                                className=' btn-floating btn-primary btn-sm ml-3'
                                onClick={() => setShowApplyEditBtn(c.name)}
                              >
                                {' '}
                                <MDBIcon icon='edit' className='text-white ' />
                              </button>
                            </>
                          )}
                        </td>
                        <td>{c.setDistanceX}</td>
                        <td>{c.setDistanceY}</td>
                      </tr>
                    ))
                  : null}
              </MDBTableBody>
            }
          </MDBTable>
        </div>
        <div className='col-1'></div>
      </div>
    </div>
  );
};

export default Countries;
