import React, { useContext, useEffect, useState } from "react";
import { Dropdown, DropdownButton, Button } from "react-bootstrap";
import AuthContext, { getToken } from "../../../providers/AuthContext";
import { BASE_URL } from "../../../common/constants";
import Side from "./../../Base/Side";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import moment from "moment";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./drop.css";
import Loading from "./../../Base/Loading";

const InfectedPeople = ({ history }) => {
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [dropValue, setDropValue] = useState("Select country");
  const [infectedPeople, setInfectedPeople] = useState([]);
  const [countriesDropMenu, setCountriesDropMenu] = useState([]);

  useEffect(() => {
    setLoading(true);
    fetch(`https://coronavirus-19-api.herokuapp.com/countries`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setCountriesDropMenu(result.map((c) => c.country));
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const setInfectedPeopleCB = (country) => {
    fetch(`${BASE_URL}/country/${country}/infectedpeople`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((r) => r.json())
      .then((result) => {
        setInfectedPeople(result);
        toast.success("Record added!", {
          position: "top-right",
          autoClose: 2000,
        });
      })
      .catch((error) => setError(error.message));
  };

  const getInfectedPeople = (ctr) => {
    setLoading(true);
    fetch(`${BASE_URL}/country/${ctr}/infectedpeople`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setInfectedPeople(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  };

  const flag = (f) => {
    return `${f.toLowerCase()} flag`;
  };

  const deleteRecord = (id) => {
    setLoading(true);
    fetch(`${BASE_URL}/country/infectedpeople/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setInfectedPeople(infectedPeople.filter((r) => r.id !== id));
        toast.error('Record deleted!', {
          position: "top-right",
          autoClose: 2000,
        });
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  };

  if (error) {
    return <h1>{error}</h1>;
  }

  if (loading) {
    return <Loading />;
  }

  return (
    <div className="container-fluid">
      <div className="row py-3">
        <ToastContainer />
        <Side />
        <div class="col" id="main">
          <h1 className="my-5">Infected people</h1>

          <DropdownButton
            onSelect={(e) => (getInfectedPeople(e), setDropValue(e))}
            className="drop"
            id="dropdown-basic-button"
            title={dropValue}
          >
            {countriesDropMenu
              .filter((c) => c !== "World")
              .sort((a, b) => a.localeCompare(b))
              .map((r, i) => (
                <Dropdown.Item key={i} eventKey={r}>
                  <i className={flag(r)}></i>
                  {r}
                </Dropdown.Item>
              ))}
          </DropdownButton>

          <Button
            className="mb-5 add"
            onClick={() => setInfectedPeopleCB(dropValue)}
          >
            Save infected people
          </Button>

          <MDBTable>
            <MDBTableHead color="blue-gradient" textWhite>
              <tr>
                <th>#</th>
                <th>Date</th>
                <th>Total infected people</th>
                <th>Delete selected record</th>
              </tr>
            </MDBTableHead>

            <MDBTableBody>
              {infectedPeople.length > 0
                ? infectedPeople
                    .sort((a, b) => new Date(b.date) - new Date(a.date))
                    .map((v, i) => (
                      <tr key={i}>
                        <td>{i + 1}</td>
                        <td>
                          {moment(v.date).format("D.MM.YYYY[г.]  HH:mm:ss[ч.]")}
                        </td>
                        <td>{v.infectedPeople}</td>
                        <td>
                          <Button
                            className="delete"
                            onClick={() => deleteRecord(v.id)}
                          >
                            Delete
                          </Button>
                        </td>
                      </tr>
                    ))
                : null}
            </MDBTableBody>
          </MDBTable>
        </div>
      </div>
      <div className="col-1"></div>
    </div>
  );
};

export default InfectedPeople;
