import React, { useContext, useEffect, useState } from 'react';
import AuthContext, { getToken } from '../../../providers/AuthContext';
import Side from '../../Base/Side';
import { BASE_URL } from '../../../common/constants';
import { Button, Dropdown, DropdownButton } from 'react-bootstrap';
import { MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import { ToastContainer } from 'react-toastify';
import { toastSuccess } from '../../../common/utils';

const Projects = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [countries, setCountries] = useState([]);
  const [countryProjects, setCountryProjects] = useState([]);
  const [dropValue, setDropValue] = useState('Select country');
  const [dropValueProjects, setDropValueProjects] = useState('Select project');
  const [dropValueUsers, setDropValueUsers] = useState('Select user');
  const [resultProjects, setresultProjects] = useState([]);
  const [users, setUsers] = useState([]);
  const [disable, setDisable] = useState(true);
  const [usersForCountry, setUsersForCountry] = useState([]);

  useEffect(() => {
    fetch(`${BASE_URL}/projects/`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setresultProjects(result);
        setCountries(
          result
            .map((r) => r.country.name)
            .filter((val, index, self) => self.indexOf(val) === index)
            .sort((a, b) => a.localeCompare(b))
        );
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const getUsers = (country) => {
    fetch(`${BASE_URL}/users/${country}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((r) => r.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setUsers(result);
      })
      .catch((error) => setError(error.message));
  };

  const assignUser = () => {
    const projectId = resultProjects.find((item) => item.name === dropValueProjects).id;
    const userId = users.find((item) => item.username === dropValueUsers).id;

    if (!projectId || !userId) {
      throw new Error('Wrong projectId or userId');
    }

    fetch(`${BASE_URL}/projects/`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify({
        projectId,
        userId,
      }),
    })
      .then((r) => r.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        toastSuccess('User assigned to this project successfully');
        setDisable(true);
        setDropValue('Select country');
        setDropValueProjects('Select project');
        setDropValueUsers('Select user');
        setCountryProjects([]);
        setUsers([]);
      })
      .catch((error) => setError(error.message));
  };
  const getCountryUsers = (country) => {
    setLoading(true);
    fetch(`${BASE_URL}/users/${country}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setUsersForCountry(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  };
  const deleteUserFromProject = (userId) => {
    fetch(`${BASE_URL}/projects/${userId}`, {
      method: `DELETE`,
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((r) => r.json())
      .then((result) => {
        setUsersForCountry(usersForCountry.filter((u) => u.id !== result.id));
        toastSuccess('User deleted succesful');
      })
      .catch((err) => setError(err.message));
  };
  const flag = (f) => {
    return `${f.toLowerCase()} flag`;
  };
  return (
    <div className="container-fluid">
      <div className="row py-3">
        <ToastContainer />
        <Side />
        <div class="col" id="main">
          <h1 className="my-5">Assign user to project</h1>
          <DropdownButton // 1 country
            onSelect={(e) => (
              setDropValue(e),
              setCountryProjects(resultProjects.filter((c) => c.country.name === e)),
              getUsers(e),
              getCountryUsers(e)
            )}
            className="drop"
            id="dropdown-basic-button"
            title={dropValue}
          >
            {countries.map((r, i) => (
              <Dropdown.Item key={i} eventKey={r}>
                <i className={flag(r)}></i>
                {r}
              </Dropdown.Item>
            ))}
          </DropdownButton>

          {countryProjects.length !== 0 ? (
            <DropdownButton // 2 projects
              onSelect={(e) => setDropValueProjects(e)}
              className="drop"
              id="dropdown-basic-button"
              title={dropValueProjects}
            >
              {countryProjects.map((r, i) => (
                <Dropdown.Item key={i} eventKey={r.name}>
                  {r.name}
                </Dropdown.Item>
              ))}
            </DropdownButton>
          ) : (
            <DropdownButton // 2 projects
              onSelect={(e) => setDropValueProjects(e)}
              className="drop"
              id="dropdown-basic-button"
              disabled
              title={dropValueProjects}
            ></DropdownButton>
          )}

          {users.length !== 0 ? (
            <DropdownButton // 3 users
              onSelect={(e) => (setDropValueUsers(e), setDisable(false))}
              className="drop"
              id="dropdown-basic-button"
              title={dropValueUsers}
            >
              {users
                .filter((u) => u.project === null)
                .map((r, i) => (
                  <Dropdown.Item key={i} eventKey={r.username}>
                    {r.username}
                  </Dropdown.Item>
                ))}
            </DropdownButton>
          ) : (
            <DropdownButton // 3 users
              // onSelect={(e) => (setDropValueUsers(e), setDisable(false))}
              className="drop"
              id="dropdown-basic-button"
              title="No users"
              disabled
            ></DropdownButton>
          )}

          <Button className="add" disabled={disable} onClick={() => assignUser()}>
            Assign user
          </Button>
          {usersForCountry.length > 0 ? (
            <MDBTable>
              <MDBTableHead color="blue-gradient" textWhite>
                <tr>
                  <th>#</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Username</th>
                  <th>Project Name</th>
                  <th>Remove from project</th>
                </tr>
              </MDBTableHead>
              <MDBTableBody>
                {usersForCountry
                  .sort((a, b) => a.project?.name - b.project?.name)
                  .map((u, i) => (
                    <tr key={i}>
                      <td>{i + 1}</td>
                      <td>{u.firstName}</td>
                      <td>{u.lastName}</td>
                      <td>{u.username}</td>
                      <td>{u.project?.name ? u.project?.name : 'No project'}</td>
                      <td>
                        {u.project?.name ? (
                          <Button className="delete" onClick={() => deleteUserFromProject(u.id)}>
                            Delete
                          </Button>
                        ) : null}
                      </td>
                    </tr>
                  ))}
              </MDBTableBody>
            </MDBTable>
          ) : null}
        </div>
      </div>
      <div className="col-1"></div>
    </div>
  );
};

export default Projects;
