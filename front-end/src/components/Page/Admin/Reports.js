import React, { useEffect, useState } from "react";
import { getToken } from "../../../providers/AuthContext";
import { BASE_URL } from "../../../common/constants";
import Side from "../../Base/Side";
import { MDBTable, MDBTableBody, MDBTableHead } from "mdbreact";
import { ToastContainer } from "react-toastify";

const Reports = () => {
  const [countries, setCountries] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/desks/report`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setCountries(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const flag = (f) => {
    return `${f.toLowerCase()} flag`;
  };

  return (
    <div className="container-fluid">
      <div className="row py-3">
        <ToastContainer />
        <Side />
        <div class="col" id="main">
          <h1 className="my-5">Desks report</h1>
          <MDBTable>
            <MDBTableHead color="blue-gradient" textWhite>
              <tr>
                <th>#</th>
                <th>Country Name</th>
                <th>Number of desks</th>
                <th>Occupied desks</th>
                <th>Forbidden desks</th>
                <th>Available desks</th>
              </tr>
            </MDBTableHead>

            <MDBTableBody>
              {countries
                ? countries.map((c, i) => (
                    <tr key={i}>
                      <td>{i + 1}</td>
                      <td>
                        <i className={flag(c.country)}></i>
                        {c.country}
                      </td>
                      <td>{c.numberOfDesks}</td>
                      <td>{c.occupiedDesks}</td>
                      <td>{c.forbiddenDesks}</td>
                      <td>{c.availableDesks}</td>
                    </tr>
                  ))
                : null}
            </MDBTableBody>
          </MDBTable>
        </div>
      </div>
      <div className="col-1"></div>
    </div>
  );
};

export default Reports;
