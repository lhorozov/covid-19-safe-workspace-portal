import React, { useContext, useEffect, useState } from "react";
import AuthContext, {
  getToken,
} from "../../../providers/AuthContext";
import { BASE_URL } from "../../../common/constants";
import {
  Button,
  Col,
  Dropdown,
  DropdownButton,
  Form,
  Row,
} from "react-bootstrap";
import "./drop.css";
import Side from "../../Base/Side";
import { ToastContainer } from "react-toastify";
import { toastSuccess, toastError } from "../../../common/utils";

const CreateCountry = ({ history }) => {
  const { user } = useContext(AuthContext);
  const [loading, setLoading] = useState(false);
  const [countriesApi, updateCountriesApi] = useState([]);
  const [countriesDataBase, updateCountriesDataBase] = useState([]);
  const [countriesDropMenu, updateCountriesDropMenu] = useState([]);
  const [dropValue, setDropValue] = useState("Select country");
  const [deskNum, setdeskNum] = useState(null);
  const [perRowNum, setPerRowNum] = useState(null);
  const [columnNum, setColumnNum] = useState(null);
  const [downTresholdNum, setDownTresholdNum] = useState(undefined);
  const [upTresholdNum, setUpTresholdNum] = useState(undefined);
  const [distBetweenCols, setDistBetweenCols] = useState(undefined);
  const [distBetweenRows, setDistBetweenRows] = useState(undefined);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    fetch(`https://coronavirus-19-api.herokuapp.com/countries`, {})
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        updateCountriesApi(result.map((r) => r.country));
        // --------------------------
        return fetch(`${BASE_URL}/country`, {
          headers: {
            Authorization: `Bearer ${getToken()}`,
            "Content-Type": "application/json",
          },
        })
          .then((response) => response.json())
          .then((result2) => {
            if (result2.error) {
              throw new Error(result2.message);
            }
            updateCountriesDataBase(result2.map((r) => r.name));
          })
          .catch((error) => setError(error.message));
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const click = () => {
    updateCountriesDropMenu(
      countriesApi.filter(function (r) {
        return this.indexOf(r) < 0;
      }, countriesDataBase)
    );
  };

  const createCountry = () => {
    if (dropValue === "Select country") {
      return toastError("Please select a country");
    }

    if (!deskNum || deskNum <= 0) {
      return toastError("Please select number of desks greater than 0");
    }

    if (!perRowNum || perRowNum <= 0 || perRowNum > deskNum) {
      return toastError("Please select correct number of desks per row");
    }
   
    if (!columnNum || columnNum < 0 || columnNum >= perRowNum) {
      return toastError("Please Select a number of desks per column between X and Y");
    }

    fetch(`${BASE_URL}/country/${dropValue}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify({
        numberOfDesks: deskNum,
        numberOfDesksPerRow: perRowNum,
        columnSpace: columnNum,
        downTresholdValue: downTresholdNum,
        upTresholdValue: upTresholdNum,
        setDistanceX: distBetweenCols,
        setDistanceY: distBetweenRows,
      }),
    })
      .then((r) => r.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        toastSuccess("Country succesfully created");
        setTimeout(() => history.push("/admin/countries"), 1500);
      })
      .catch((error) => setError(error.message));
  };

  const flag = (f) => {
    return `${f.toLowerCase()} flag`;
  };

  return (
    <div className="container-fluid">
      <div className="row py-3">
        <ToastContainer />
        <Side />
        <div className="col" id="main">
          <h1 className="my-5">Create country</h1>
          <DropdownButton
            onSelect={(e) => setDropValue(e)}
            className="drop-create"
            onClick={click}
            id="dropdown-basic-button-create"
            title={dropValue}
          >
            {countriesDropMenu
              .filter(c=> c !=="World").sort((a, b) => a.localeCompare(b))
              .map((r, i) => (
                <Dropdown.Item key={i} eventKey={r}>
                  <i className={flag(r)}></i>
                  {r}
                </Dropdown.Item>
              ))}
          </DropdownButton>
          <div className="jumbotron">
            <Form>
              <Form.Group as={Row} controlId="formPlaintextPassword">
                <Form.Label column sm="2">
                  Number of Desks:
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="number"
                    placeholder=""
                    onChange={(e) => setdeskNum(e.target.value)}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row} controlId="formPlaintextPassword">
                <Form.Label column sm="2">
                  Number of Desks per row:
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="number"
                    placeholder=""
                    onChange={(e) => setPerRowNum(e.target.value)}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row} controlId="formPlaintextPassword">
                <Form.Label column sm="2">
                  Space between columns
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="number"
                    placeholder=""
                    onChange={(e) => setColumnNum(e.target.value)}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row} controlId="formPlaintextPassword">
                <Form.Label column sm="2">
                  Treshold: down
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="number"
                    step=".01"
                    placeholder="Default number is 5%"
                    onChange={(e) => setDownTresholdNum(e.target.value)}
                  />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId="formPlaintextPassword">
                <Form.Label column sm="2">
                  Treshold: up
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="number"
                    step=".01"
                    placeholder="Default number is 10%"
                    onChange={(e) => setUpTresholdNum(e.target.value)}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row} controlId="formPlaintextPassword">
                <Form.Label column sm="2">
                  Distance between columns
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="number"
                    step=".01"
                    placeholder="Default number is 3m"
                    onChange={(e) => setDistBetweenCols(e.target.value)}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row} controlId="formPlaintextPassword">
                <Form.Label column sm="2">
                  Distance between rows
                </Form.Label>
                <Col sm="10">
                  <Form.Control
                    type="number"
                    step=".01"
                    placeholder="Default number is 2m"
                    onChange={(e) => setDistBetweenRows(e.target.value)}
                  />
                </Col>
              </Form.Group>
            </Form>
                      <Button className="add" onClick={createCountry}>
              Create country
            </Button>
          </div>
        </div>
      <div className="col-1">
      </div>
      </div>
    </div>
  );
};

export default CreateCountry;
