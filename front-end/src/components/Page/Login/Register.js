import React, { useState, useEffect } from "react";
import { BASE_URL } from "../../../common/constants";
import { Button, Form, Col } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import "./Register.css";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBIcon,
} from "mdbreact";
import { toastError, toastSuccess } from "../../../common/utils";
import { ToastContainer } from "react-toastify";

const Register = ({ history }) => {
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [countries, setCountries] = useState([]);

  const [user, setUserObject] = useState({
    username: {
      value: "",
      touched: false,
      valid: true,
    },
    firstName: {
      value: "",
      touched: false,
      valid: true,
    },
    lastName: {
      value: "",
      touched: false,
      valid: true,
    },
    password: {
      value: "",
      touched: false,
      valid: true,
    },
    confirmPassword: {
      value: "",
      touched: false,
      valid: true,
    },
    email: {
      value: "",
      touched: false,
      valid: true,
    },
    country: {
      value: "",
      touched: false,
      valid: true,
    },
  });

  useEffect(() => {
    setLoading(true); // Promise.allSetled
    fetch(`${BASE_URL}/country`, {})
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setCountries(result);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const userValidators = {
    username: [
      (value) => value?.length >= 5 || "Username must be 5 characters or more",
      (value) =>
        value?.length <= 15 || "Username must be 15 characters or less",
      (value) =>
        /^[\x00-\x7F]*$/gim.test(value) ||
        "Username must includes english alphabet only",
    ],
    password: [
      (value) => value?.length >= 5 || "Password must be 5 characters or more",
      (value) =>
        value?.length <= 15 || "Password must be 15 characters or less",
      (value) =>
        /[a-z]/gim.test(value) ||
        "Password must includes at least one a-z letter",
      (value) =>
        /[0-9]/gim.test(value) || "Password must includes at least one number",
      (value) =>
        /^[\x00-\x7F]*$/gim.test(value) ||
        "Password must includes english alphabet only",
    ],
    firstName: [
      (value) =>
        value?.length >= 2 || "First name must be 2 characters or more",
      (value) =>
        value?.length <= 15 || "First name must be 15 characters or less",
      (value) =>
        /^[\x00-\x7F]*$/gim.test(value) ||
        "Password must includes english alphabet only",
    ],
    lastName: [
      (value) => value?.length >= 2 || "Last name must be 2 characters or more",
      (value) =>
        value?.length <= 50 || "Last name must be 15 characters or less",
      (value) =>
        /^[\x00-\x7F]*$/gim.test(value) ||
        "Last name must includes english alphabet only",
    ],
    confirmPassword: [
      (value) =>
        value === user.password.value ||
        "Confirm password must be the same as password",
    ],
    email: [
      (value) =>
        /(.+)@(.+){2,}\.(.+){2,}/gim.test(value) || "Email must be valid",
    ],
    country: [() => true],
  };

  const getValidationErrors = (prop) => {
    return userValidators[prop]
      .map((fn) => fn(user[prop].value))
      .filter((value) => typeof value === "string");
  };

  const updateUser = (prop, value) =>
    setUserObject({
      ...user,
      [prop]: {
        value,
        touched: true,
        valid: userValidators[prop].reduce(
          (isValid, valFn) => isValid && typeof valFn(value) !== "string",
          true
        ),
      },
    });

  const classNames = (prop) => {
    let classes = "";
    if (user[prop].touched) {
      classes += "touched ";

      if (user[prop].valid) {
        classes += "valid ";
      } else {
        classes += "invalid ";
      }
    }

    return classes;
  };

  const validateForm = () =>
    !Object.keys(user).reduce(
      (isValid, prop) => isValid && user[prop].valid && user[prop].touched,
      true
    );

  const register = (e) => {
    e.preventDefault();

    fetch(`${BASE_URL}/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: user.username.value,
        firstName: user.firstName.value,
        lastName: user.lastName.value,
        password: user.password.value,
        confirmPassword: user.confirmPassword.value,
        email: user.email.value,
        country: user.country.value,
      }),
    })
      .then((r) => r.json())
      .then((result) => {
        if (result.error) {
          toastError(result.message);
        } else {
          toastSuccess("Successful registration");
          setTimeout(() => {
            return history.push("/login");
          }, 1500);
        }
      })
      .catch((error) => toastError(error.message));
  };

  if (loading) {
    return <h1>Loading...</h1>;
  }

  // if (error) {
  //   return <h3>{error}</h3>;
  // }

  const handleCountryChange = (e) => {
    const id = countries.find((country) => country.name === e.target.value)?.id;

    updateUser("country", id);
  };

  return (
    <div className="container">
      <ToastContainer />
      <MDBCard className="my-5">
        <MDBCardBody>
          <MDBCardHeader className="form-header deep-blue-gradient rounded">
            <h3 className="my-3">
              <MDBIcon icon="address-card" className="text-white" /> Register:
            </h3>
          </MDBCardHeader>

          <Form>
            <Form.Row className="mt-3">
              <Form.Group as={Col}>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  id="email"
                  className={classNames("email")}
                  value={user.email.value}
                  onChange={(e) => {
                    updateUser("email", e.target.value);
                  }}
                />
                {user.email.touched && !user.email.valid
                  ? getValidationErrors("email").map((error, index) => (
                      <p key={index} className="validation-message">
                        {error}
                      </p>
                    ))
                  : null}
              </Form.Group>
            </Form.Row>

            <Form.Row className="mt-3">
              <Form.Group as={Col} md="4">
                <Form.Label>Username</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Username"
                  id="input-username"
                  value={user.username.value}
                  className={classNames("username")}
                  onChange={(e) => updateUser("username", e.target.value)}
                />
                {user.username.touched && !user.username.valid
                  ? getValidationErrors("username").map((error, index) => (
                      <p key={index} className="validation-message">
                        {error}
                      </p>
                    ))
                  : null}
              </Form.Group>

              <Form.Group as={Col}>
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  value={user.password.value}
                  id="pass"
                  className={classNames("password")}
                  onChange={(e) => {
                    updateUser("password", e.target.value);
                  }}
                />
                {user.password.touched && !user.password.valid
                  ? getValidationErrors("password").map((error, index) => (
                      <p key={index} className="validation-message">
                        {error}
                      </p>
                    ))
                  : null}
              </Form.Group>

              <Form.Group as={Col}>
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Confirm Password"
                  id="confirm-pass"
                  className={classNames("confirmPassword")}
                  value={user.confirmPassword.value}
                  onChange={(e) => {
                    updateUser("confirmPassword", e.target.value);
                  }}
                />
                {user.confirmPassword.touched && !user.confirmPassword.valid
                  ? getValidationErrors("confirmPassword").map(
                      (error, index) => (
                        <p key={index} className="validation-message">
                          {error}
                        </p>
                      )
                    )
                  : null}
              </Form.Group>
            </Form.Row>

            <Form.Row className="mt-3">
              <Form.Group as={Col} md="4">
                <Form.Label>First name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="First name"
                  id="input-firstname"
                  value={user.firstName.value}
                  className={classNames("firstName")}
                  onChange={(e) => updateUser("firstName", e.target.value)}
                />
                {user.firstName.touched && !user.firstName.valid
                  ? getValidationErrors("firstName").map((error, index) => (
                      <p key={index} className="validation-message">
                        {error}
                      </p>
                    ))
                  : null}
              </Form.Group>

              <Form.Group as={Col} md="4">
                <Form.Label>Last name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Last name"
                  id="input-lastName"
                  value={user.lastName.value}
                  className={classNames("lastName")}
                  onChange={(e) => updateUser("lastName", e.target.value)}
                />
                {user.lastName.touched && !user.lastName.valid
                  ? getValidationErrors("lastName").map((error, index) => (
                      <p key={index} className="validation-message">
                        {error}
                      </p>
                    ))
                  : null}
              </Form.Group>

              <Form.Group as={Col}>
                <Form.Label>Country</Form.Label>
                <Form.Control as="select" onChange={handleCountryChange}>
                  <option key={0}>Select country</option>
                  {countries.map((country, index) => (
                    <option key={index + 1}>
                      {country.name.substring(0, 1).toUpperCase() +
                        country.name.substring(1).toLowerCase()}
                    </option>
                  ))}
                </Form.Control>
              </Form.Group>
            </Form.Row>

            <Button
              onClick={register}
              disabled={validateForm() || user.country.value === undefined}
              variant="primary"
              type="submit"
              className="btn mr-4 mt-5 mb-3"
            >
              Submit
            </Button>
            <Button
              onClick={() => history.push("/home")}
              className="btn mr-3 mb-3 mt-5"
            >
              Cancel
            </Button>
          </Form>
        </MDBCardBody>
      </MDBCard>
    </div>
  );
};

export default withRouter(Register);
