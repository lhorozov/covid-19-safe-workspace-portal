import React, { useState, useContext } from "react";
import { BASE_URL } from "../../../common/constants";
import AuthContext, { getToken } from "../../../providers/AuthContext";
import jwtDecode from "jwt-decode";
import { Button } from "react-bootstrap";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBIcon,
  MDBCardHeader,
  MDBInput,
} from "mdbreact";
import "./login.css";
import { toastError } from "../../../common/utils";
import { ToastContainer } from "react-toastify";

const Login = ({ history }) => {
  const [user, setUserObject] = useState({
    username: {
      value: "",
      touched: false,
      valid: true,
    },
    email: {
      value: "",
      touched: false,
      valid: true,
    },
    password: {
      value: "",
      touched: false,
      valid: true,
    },
  });
  const [error, setError] = useState(null);

  const { setLoginState } = useContext(AuthContext);

  const updateUser = (prop, value) =>
    setUserObject({
      ...user,
      [prop]: {
        value,
        touched: true,
        valid: userValidators[prop].reduce(
          (isValid, valFn) => isValid && typeof valFn(value) !== "string",
          true
        ),
      },
    });

  const userValidators = {
    username: [
      (value) => value?.length >= 5 || "Username must be 5 characters or more",
      (value) =>
        value?.length <= 15 || "Username must be 15 characters or less",
      (value) =>
        /^[\x00-\x7F]*$/gim.test(value) ||
        "Username must includes english alphabet only",
    ],
    password: [
      (value) => value?.length >= 5 || "Password must be 5 characters or more",
      (value) =>
        value?.length <= 15 || "Password must be 15 characters or less",
      (value) =>
        /[a-z]/gim.test(value) ||
        "Password must includes at least one a-z letter",
      (value) =>
        /[0-9]/gim.test(value) || "Password must includes at least one number",
      (value) =>
        /^[\x00-\x7F]*$/gim.test(value) ||
        "Password must includes english alphabet only",
    ],
    email: [
      (value) =>
        /(.+)@(.+){2,}\.(.+){2,}/gim.test(value) || "Email must be valid",
    ],
    country: [() => true],
  };

  const classNames = (prop) => {
    let classes = "";
    if (user[prop].touched) {
      classes += "touched ";

      if (user[prop].valid) {
        classes += "valid ";
      } else {
        classes += "invalid ";
      }
    }

    return classes;
  };

  const login = (e) => {
    if (
      !/^[\x00-\x7F]*$/.test(user.username) ||
      !/^[\x00-\x7F]*$/.test(user.password)
    ) {
      return toastError("You have to be using english alphabet only");
    }

    fetch(`${BASE_URL}/session`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify({
        email: user.email.value,
        password: user.password.value,
      }),
    })
      .then((r) => r.json())
      .then((result) => {
        if (result.error) {
          setUserObject({
            username: {
              value: "",
              touched: false,
              valid: true,
            },
            email: {
              value: "",
              touched: false,
              valid: true,
            },
            password: {
              value: "",
              touched: false,
              valid: true,
            },
          });
          throw new Error(result.message);
        }
        try {
          const payload = jwtDecode(result.token);
          setLoginState({
            isLoggedIn: true,
            user: payload,
          });
        } catch (e) {
          return toastError(e.message);
        }
        localStorage.setItem("token", result.token);
        history.push("/home");
      })
      .catch((error) => setError(error.message));
  };

  return (
    <div className="container">
      <ToastContainer />
      <div className="login">
        <MDBContainer className="mt-5">
          <MDBRow>
            <MDBCol md="6">
              <MDBCard>
                <MDBCardBody>
                  <MDBCardHeader className="form-header deep-blue-gradient rounded">
                    <h3 className="my-3">
                      <MDBIcon icon="lock" /> Login:
                    </h3>
                  </MDBCardHeader>
                  <form>
                    <div className="grey-text">
                      <MDBInput
                        label="Type your email"
                        icon="envelope"
                        group
                        type="email"
                        validate
                        error="wrong"
                        success="right"
                        id="email"
                        className={classNames("email")}
                        value={user.email.value}
                        onChange={(e) => {
                          updateUser("email", e.target.value);
                        }}
                      />
                      <MDBInput
                        label="Type your password"
                        icon="lock"
                        group
                        type="password"
                        validate
                        id="pass"
                        className={classNames("password")}
                        onChange={(e) => {
                          updateUser("password", e.target.value);
                        }}
                      />
                    </div>
                  </form>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
        {error ? <p className="validation-message">{error}</p> : null} <br />
        <Button
          onClick={login}
          variant="primary"
          type="submit"
          className="btn mr-4 mt-5 add"
        >
          Login
        </Button>
        <Button onClick={() => history.push("/home")} className="btn mr-4 mt-5">
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default Login;
