export class JWTPayload {
	id: number;
	username: string;
	email: string;
	role: string;
	country: string;
	project: string;
}
