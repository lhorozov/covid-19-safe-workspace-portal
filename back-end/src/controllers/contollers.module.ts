import { Module } from '@nestjs/common';
import { DesksController } from './desks.controller';
import { ServicesModule } from 'src/services/services.module';
import { UsersController } from './users.controller';
import { AuthController } from './auth.controller';
import { CountryController } from './country.controller';
import { ProjectsController } from './projects.controller';

@Module({
  imports: [ServicesModule],
  controllers: [DesksController, UsersController, AuthController, CountryController, ProjectsController],
})
export class ControllersModule {}
