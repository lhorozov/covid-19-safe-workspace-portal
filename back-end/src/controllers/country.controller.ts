import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { ReturnMessageDTO } from 'src/dtos/return-msg.dto';
import { RequestCountryDTO } from './../dtos/country.dto';
import { CountryService } from 'src/services/country.service';
import { DeskPlanningDTO } from 'src/dtos/desk-planning.dto';
import { userToProjectDTO } from 'src/dtos/usertoproject-dto';
import { Country } from 'src/models/country.entity';
import { ThisWeekMatrix } from 'src/models/this-week-matrix.entity';
import { InfectedPeople } from 'src/models/infected-people.entity';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { changeTreshold } from 'src/dtos/change-treshold.dto';
import { ApiData } from 'src/models/api-data.entity';

@Controller('country')
export class CountryController {
	constructor(private readonly countryService: CountryService) {}

	@Get()
	async getAllCountries(): Promise<Country[]> {
		return await this.countryService.getAllCountries();
	}

	@Post('/:country/schedule')
	async setCountrySchedule(@Param('country') country: string): Promise<ReturnMessageDTO> {
		return await this.countryService.setCountrySchedule(country);
	}

	@Get('/:country/schedule')
	async getCountrySchedule(@Param('country') country: string): Promise<string[][]> {
		return await this.countryService.getCountrySchedule(country);
	}

	@Get('/:country/weektotalcases')
	async getWeekTotalCases(@Param('country') country: string): Promise<Partial<ApiData>> {
		return await this.countryService.getWeekTotalCases(country);
	}

	@Post('/:country/infectedpeople')
	@UseGuards(BlacklistGuard)
	async addInfectedPeople(@Param('country') country: string): Promise<InfectedPeople[]> {
		return await this.countryService.addInfectedPeople(country);
	}

	@Get('/:country/infectedpeople')
	async getInfectedPeople(@Param('country') country: string): Promise<InfectedPeople[]> {
		return await this.countryService.getInfectedPeople(country);
	}

	@Delete('/infectedpeople/:id')
	async removeSelectedRecordByID(@Param('id') id: string,): Promise<ReturnMessageDTO> {
		return await this.countryService.removeSelectedRecordByID(id);
	}

	@Get('/schedule')
	async getAllCountrySchedules(): Promise<string[]> {
		return await this.countryService.getAllCountrySchedules();
	}

	@Get('/:country/nextschedule')
	async setCountryScheduleNextWeek(@Param('country') country: string): Promise<string[][]> {
		return await this.countryService.setCountryScheduleNextWeek(country);
	}
	
	@Post('/:country')
	@UseGuards(BlacklistGuard)
	async createCountry(
		@Param('country') country: string,
		@Body() deskPlanning: DeskPlanningDTO
	): Promise<ReturnMessageDTO> {
		return this.countryService.createCountry(country, deskPlanning);
	}

	@Put('/:country')
	@UseGuards(BlacklistGuard)
	async changeTreshold(
		@Param('country') country: string,
		@Body() tresholds: changeTreshold
	): Promise<Country> {
		return this.countryService.changeTreshold(country, tresholds);
	}
	

}
