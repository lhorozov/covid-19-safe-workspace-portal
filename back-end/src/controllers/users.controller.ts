import { Controller, Get, Param, Post, Body, UseGuards, Delete } from '@nestjs/common';
import { UsersService } from 'src/services/users.service';
import { AuthService } from '../services/auth.service';
import { User } from 'src/models/users.entity';
import { requestUserDTO } from 'src/dtos/users/request-user.dto';
import { ResponseMessageDTO } from 'src/dtos/users/response-msg.dto';
import { VacationDataDTO } from 'src/dtos/users/vacationData.dto';
import { Vacation } from 'src/models/vacation.entity';
import { UserId } from 'src/auth/user-id.decorator';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { ReturnMessageDTO } from 'src/dtos/return-msg.dto';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/models/enums/user-role';

@Controller('users')
export class UsersController {
	constructor(private readonly usersService: UsersService, private readonly authService: AuthService) {}

	@Get()
	@UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
	async getAllUsers(): Promise<User[]> {
		return await this.usersService.getAllUsers();
	}

	@Get('/vacation')
	@UseGuards(BlacklistGuard)
	async getVacationForUser(@UserId() userId: number): Promise<Vacation[]> {
		return await this.usersService.getVacationForUser(userId);
	}

	@Get('/:country')
	@UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
	async getAllUsersFromCountry(@Param('country') country: string): Promise<User[]> {
		return await this.usersService.getAllUsersFromCountry(country);
	}

	@Post()
	async registerUser(@Body() user: requestUserDTO): Promise<ResponseMessageDTO> {
		await this.usersService.registerUser(user);
		return { msg: 'User Added!' };
	}

	@Post('/vacation')
	@UseGuards(BlacklistGuard)
	async setVacationToUser(@Body() input: VacationDataDTO, @UserId() userId: number): Promise<Vacation[]> {
		return await this.usersService.setVacationToUser(input, userId);
	}

	@Delete(':id/vacation/')
	@UseGuards(BlacklistGuard)
	async deleteVacationToUser(@Param('id') vacId: number, @UserId() userId: number): Promise<ReturnMessageDTO> {
		return await this.usersService.deleteVacationToUser(vacId, userId);

	}

	
}
