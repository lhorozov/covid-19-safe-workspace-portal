import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { DesksService } from 'src/services/desks.service';
import { RequestCountryDTO } from './../dtos/country.dto';

@Controller('desks')
export class DesksController {
	constructor(private readonly desksService: DesksService) {}

	@Get('/all')
	async getApiData(): Promise<Partial<RequestCountryDTO[]>> {
		return await this.desksService.getApiData();
	}

	@Get('/report')
	@UseGuards(BlacklistGuard)
	async getDesksReport(): Promise<RequestCountryDTO[]> {
		return await this.desksService.getDesksReport();
	}

	@Get('/:country')
	async getApiDataByCountry(@Param('country') country: string): Promise<RequestCountryDTO[]> {
		return await this.desksService.getApiDataByCountry(country);
	}

}
