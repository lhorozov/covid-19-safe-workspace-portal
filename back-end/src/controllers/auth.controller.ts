import { Controller, Body, Post } from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { UserLoginDTO } from '../dtos/users/login-user.dto';


@Controller('session')
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Post()
  async login(@Body() user: UserLoginDTO): Promise<{ token: string }> {
    return await this.authService.login(user);
  }

}
