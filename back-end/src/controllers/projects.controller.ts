import { Controller, Get, Param, Post, Body, Put, Delete, UseGuards } from '@nestjs/common';
import { ProjectsService } from 'src/services/projects.service';
import { Project } from 'src/models/project.entity';
import { createProjectDTO } from 'src/dtos/create-project.dto';
import { ReturnMessageDTO } from 'src/dtos/return-msg.dto';
import { ReturnProjectDTO } from 'src/dtos/return-project.dto';
import { userToProjectDTO } from 'src/dtos/usertoproject-dto';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { User } from 'src/models/users.entity';

@Controller('projects')
export class ProjectsController {
	constructor(private readonly projectsService: ProjectsService) {}

	@Get()
	async getAllCountryData(): Promise<Partial<Project[]>> {
		return await this.projectsService.allProjects();
	}

	@Get('/:country')
	async allProjectsForCountry(@Param('country') country: string): Promise<Partial<Project[]>> {
		return await this.projectsService.allProjectsForCountry(country);
	}

	@Post('/:country')
	@UseGuards(BlacklistGuard)
	async createProject(
		@Param('country') country: string,
		@Body() createProjectParams: createProjectDTO
	): Promise<ReturnProjectDTO> {
		return this.projectsService.createProject(country, createProjectParams);
	}

	@Put()
	@UseGuards(BlacklistGuard)
	async addUserToProject(@Body() input: userToProjectDTO): Promise<ReturnMessageDTO> {
		return this.projectsService.addUserToProject(input);
	}

	@Delete(`/:id`)
	async deleteUserFromProject(@Param('id') userId: number): Promise<User> {
		return this.projectsService.deleteUserFromProject(userId);
 
	}
}
