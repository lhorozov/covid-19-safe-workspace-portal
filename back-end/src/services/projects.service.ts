import { Injectable, BadRequestException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Project } from 'src/models/project.entity';
import { Country } from 'src/models/country.entity';
import { ReturnMessageDTO } from 'src/dtos/return-msg.dto';
import { ReturnProjectDTO } from 'src/dtos/return-project.dto';
import { User } from 'src/models/users.entity';
import { userToProjectDTO } from 'src/dtos/usertoproject-dto';

@Injectable()
export class ProjectsService {
	constructor(
		@InjectRepository(Project) private readonly projectsRepository: Repository<Project>,
		@InjectRepository(Country) private readonly countryRepository: Repository<Country>,
		@InjectRepository(User) private readonly userRepository: Repository<User>,
		@InjectRepository(Project) private readonly projectRepository: Repository<Project>,
	) {}

	async allProjects(): Promise<Project[]> {
		const projects = await this.projectsRepository.find({relations: [`country`, `users`]});
		if (!projects) {
			throw new BadRequestException('There are some error about getting all projects');
		}
		return projects;
	}

	async allProjectsForCountry(country: string): Promise<Project[]> {
		const countryID = await (await this.countryRepository.findOne({ where: { name: country } })).id;
		if (!countryID) {
			throw new BadRequestException('There are some error about getting country');
		}

		const projects = await this.projectsRepository.find({ where: { country: countryID } });
		if (!projects) {
			throw new BadRequestException('There are some error about getting all projects');
		}

		return projects;
	}

	async createProject(countryString, createProjectParams): Promise<ReturnProjectDTO> {
		
    const { name, description } = createProjectParams;
    const country = await (await this.countryRepository.findOne({ where: { name: countryString } }));
    
    if (!country) {
			throw new BadRequestException('There are some error about getting country');
		}

		const project = await this.projectsRepository.findOne({ where: { name } });
		if (project) {
			throw new BadRequestException('Project with that name has already exists');
    }

    const projectToCreate = {
      name,
      description,
      country
    }
    
    await this.projectsRepository.save(projectToCreate);

		return {
			name,
			description,
		};
	}

	async addUserToProject(input: userToProjectDTO): Promise<ReturnMessageDTO> {
		const user = await this.userRepository.findOne({
			where: { id: input.userId }
		});
		if (!user) {
			throw new BadRequestException('no user');
		}
		const project = await this.projectRepository.findOne({
			where: { id: input.projectId }
		});
		if (!project) {
			throw new BadRequestException('no project');
		}
		user.project = project;
		await this.userRepository.save(user);

		return { msg: 'Project added to User' };
	}
	
	async deleteUserFromProject(userId: number): Promise<User> {
		const user = await this.userRepository.findOne({
			where: { id: userId }, relations: ['project']
		});
		if (!user) {
			throw new BadRequestException('no user');
		}
		user.project = null;
		
		await this.userRepository.save(user);

		return user;
	}
}
