import { Injectable, BadRequestException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from 'src/models/users.entity';
import { requestUserDTO } from 'src/dtos/users/request-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Country } from 'src/models/country.entity';
import { Vacation } from 'src/models/vacation.entity';
import { ReturnMessageDTO } from 'src/dtos/return-msg.dto';
import { CountryService } from './country.service';

@Injectable()
export class UsersService {
	constructor(
		private readonly countryService: CountryService,
		@InjectRepository(User) private readonly usersRepository: Repository<User>,
		@InjectRepository(Country) private readonly countryRepository: Repository<Country>,
		@InjectRepository(Vacation) private readonly vacationRepository: Repository<Vacation>
	) {}

	async registerUser(body: requestUserDTO): Promise<User> {
		const users = await this.usersRepository.find();

		if (users.some((u) => u.username === body.username)) {
			throw new BadRequestException('Username already exists');
		}

		if (users.some((u) => u.email === body.email)) {
			throw new BadRequestException('Email already exists');
		}

		const newUser = await this.usersRepository.create(body);
		newUser.password = await bcrypt.hash(newUser.password, 10);
		return this.usersRepository.save(newUser);
	}

	async getAllUsersFromCountry(country: string): Promise<User[]> {
		const id = await (await this.countryRepository.findOne({ where: { name: country } })).id;
		const users = await this.usersRepository.find({ where: { country: id }, relations: [`project`] });
		return users;
	}

	async getAllUsers(): Promise<User[]> {
		const users = await this.usersRepository.find();
		return users;
	}

	async setVacationToUser(input, userId): Promise<Vacation[]> {
		const user = await this.usersRepository.findOne({ where: { id: userId }, relations: ['country']  });

		if (!user) {
			throw new BadRequestException('No such user exists');
		}

		const vacation = await this.vacationRepository.create({
			startDate: input.startDate,
			endDate: input.endDate,
			user
		});

		await this.vacationRepository.save(vacation);
		const result = await this.vacationRepository.find({ where: { user: userId, isDeleted: false  }, relations: ['user'] });

		return result;
	}

	async getVacationForUser(userId: number): Promise<Vacation[]> {
		const user = await this.usersRepository.findOne({ where: { id: userId } });

		if (!user) {
			throw new BadRequestException('No such user exists');
		}

		const vacation = await this.vacationRepository.find({ where: { user: userId, isDeleted: false } });

		return vacation;
	}
	
	async deleteVacationToUser(vacId: number, userId: number): Promise<ReturnMessageDTO> {
		const user = await this.usersRepository.findOne({ where: { id: userId }, relations: ['country'] });
	
		if (!user) {
			throw new BadRequestException('No such user exists');
		}

		const vacation = await this.vacationRepository.findOne({ where: { id: vacId } });
		vacation.isDeleted = true;
		await this.vacationRepository.save(vacation)
		await this.countryService.setCountrySchedule(user.country.name);

		return {msg: 'Vacation is deleted!'};
	}
}
