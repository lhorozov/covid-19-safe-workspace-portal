import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/users.entity';
import * as bcrypt from 'bcrypt';
import { UserLoginDTO } from 'src/dtos/users/login-user.dto';
import { JWTPayload } from 'src/common/jwt-payload';
import { UserRole } from 'src/models/enums/user-role';

@Injectable()
export class AuthService {
	constructor(
		@InjectRepository(User) private readonly userRepository: Repository<User>,
		private readonly jwtService: JwtService
	) {}

	async findUserByEmail(email: string): Promise<User> {
		return await this.userRepository.findOne({
			where: {
				email,
				isDeleted: false
			},
			relations: ['country', 'project']
		});
	}

	async validateUser(email: string, password: string): Promise<User> {
		const user = await this.findUserByEmail(email);
		if (!user) {
			return null;
		}
		const isUserValidated = await bcrypt.compare(password, user.password);
		return isUserValidated ? user : null;
	}

	async login(loginUser: UserLoginDTO): Promise<{ token: string }> {
		const user = await this.validateUser(loginUser.email, loginUser.password);
		if (!user) {
			throw new UnauthorizedException('The email or password you entered is incorrect.');
		}
		const payload: JWTPayload = {
			id: user.id,
			username: user.username,
			email: user.email,
			role: UserRole[user.role],
			country: user.country?.name,
			project: user.project?.name,
		};
		const token = await this.jwtService.signAsync(payload);
		return {
			token
		};
	}
}
