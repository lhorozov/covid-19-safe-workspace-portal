import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { DesksService } from './desks.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransformService } from './transform.service';
import { User } from 'src/models/users.entity';
import { jwtConstants } from 'src/constant/secret';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt-strategy';
import { Desk } from 'src/models/desks.entity';
import { Project } from 'src/models/project.entity';
import { Country } from 'src/models/country.entity';
import { ApiData } from 'src/models/api-data.entity';
import { CountryService } from './country.service';
import { Matrix } from 'src/models/matrix.entity';
import { ThisWeekMatrix } from 'src/models/this-week-matrix.entity';
import { ProjectsService } from './projects.service';
import { Vacation } from 'src/models/vacation.entity';
import { InfectedPeople } from 'src/models/infected-people.entity';

@Module({
	imports: [
		TypeOrmModule.forFeature([
			User,
			Desk,
			Country,
			Project,
			ApiData,
			Matrix,
			Project,
			ThisWeekMatrix,
			Vacation,
			InfectedPeople
		]),
		PassportModule,
		JwtModule.register({
			secret: jwtConstants.secret,
			signOptions: {
				expiresIn: '7d'
			}
		})
	],
	providers: [UsersService, DesksService, TransformService, AuthService, JwtStrategy, CountryService, ProjectsService],
	exports: [UsersService, DesksService, TransformService, AuthService, CountryService, ProjectsService]
})
export class ServicesModule {}
