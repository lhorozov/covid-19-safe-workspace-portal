import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ApiData } from 'src/models/api-data.entity';
import { ReturnMessageDTO } from 'src/dtos/return-msg.dto';
import { Country } from 'src/models/country.entity';
import { Desk } from 'src/models/desks.entity';
import { DeskPlanningDTO } from 'src/dtos/desk-planning.dto';
import { User } from 'src/models/users.entity';
import { Matrix } from 'src/models/matrix.entity';
import { ThisWeekMatrix } from 'src/models/this-week-matrix.entity';
import { getConnection } from 'typeorm';
import { InfectedPeople } from 'src/models/infected-people.entity';
import fetch from 'node-fetch';
import { changeTreshold } from 'src/dtos/change-treshold.dto';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class CountryService {
	constructor(
		@InjectRepository(Country)
		private readonly countryRepository: Repository<Country>,
		@InjectRepository(Desk)
		private readonly deskRepository: Repository<Desk>,
		@InjectRepository(User)
		private readonly userRepository: Repository<User>,
		@InjectRepository(Matrix)
		private readonly matrixRepository: Repository<Matrix>,
		@InjectRepository(InfectedPeople)
		private readonly infectedPeopleRepository: Repository<InfectedPeople>,
		@InjectRepository(ApiData)
		private readonly apiDataRepository: Repository<ApiData>,
		@InjectRepository(ThisWeekMatrix)
		private readonly thisWeekMatrixRepository: Repository<ThisWeekMatrix>
	) {}

	@Cron('0 30 19 * * 5') // Every friday at 19:30h
	async renderThisWeek() {
		await (await this.countryRepository.find()).map((c) => this.setCountrySchedule(c.name));
	}

	async getAllCountries(): Promise<Country[]> {
		const countries = await this.countryRepository.find();
		if (!countries) {
			throw new BadRequestException('no countries to show');
		}
		return countries;
	}

	async createCountry(country: string, deskPlanning: DeskPlanningDTO): Promise<ReturnMessageDTO> {
		const checkCountry = await this.countryRepository.findOne({
			where: { name: country }
		});

		if (checkCountry) {
			throw new BadRequestException('Country already exists');
		}

		const countryObj = {
			name: country,
			numberOfDesks: deskPlanning.numberOfDesks,
			numberOfDesksPerRow: deskPlanning.numberOfDesksPerRow,
			columnSpace: deskPlanning.columnSpace,
			downTresholdValue: deskPlanning?.downTresholdValue,
			upTresholdValue: deskPlanning?.upTresholdValue,
			setDistanceX: deskPlanning?.setDistanceX,
			setDistanceY: deskPlanning?.setDistanceY
		};

		const countryToCreate = await this.countryRepository.create(countryObj);
		if (!countryToCreate) {
			throw new BadRequestException('bad inputs');
		}

		await this.countryRepository.save(countryToCreate);
		const createdCountry = await this.countryRepository.findOne({
			where: { name: country }
		});

		for (let index = 0; index < deskPlanning.numberOfDesks; index++) {
			const newRowObj = {
				country: createdCountry
			};
			const newRow = await this.deskRepository.create(newRowObj);

			await this.deskRepository.save(newRow);
		}
		const rows = Math.ceil(deskPlanning.numberOfDesks / deskPlanning.numberOfDesksPerRow);

		let counterPeople = 0;
		for (let index = 0; index < rows; index++) {
			let setValue = '';

			for (let j = 0; j < deskPlanning.numberOfDesksPerRow; j++) {
				if (counterPeople >= deskPlanning.numberOfDesks) {
					setValue += '0';
				} else {
					counterPeople++;
					setValue += `1`;
				}
			}

			const rowToCreate = {
				row: index,
				value: [setValue.split('').join(',')],
				country: createdCountry
			};

			const newRowForMatrix = await this.matrixRepository.create(rowToCreate);
			await this.matrixRepository.save(newRowForMatrix);
		}
		return { msg: 'Database updated' };
	}

	isOnVacationThisWeek = (obj, isThisWeek) => {
		function addDays(date, days) {
			const result = new Date(date);
			result.setDate(result.getDate() + days);
			return result;
		}

		let monday = new Date();
		monday.setDate(monday.getDate() + ((1 + 7 - monday.getDay()) % 7));

		let friday = addDays(monday, 4);

		if (!isThisWeek) {
			monday = addDays(monday, 7);
			friday = addDays(monday, 11);
		}

		const startDateNumber = new Date(obj.startDate).getTime();
		const endDateNumber = new Date(obj.endDate).getTime();
		const mondayNumber = new Date(monday).getTime();
		const fridayNumber = new Date(friday).getTime();

		return (
			(mondayNumber <= startDateNumber && startDateNumber <= fridayNumber) ||
			(mondayNumber <= endDateNumber && endDateNumber <= fridayNumber)
		);
	};

	async setCountrySchedule(country: string): Promise<ReturnMessageDTO> {
		const countryDB = await this.countryRepository.findOne({ where: { name: country } });

		const checkForCountryThisWeek = await this.thisWeekMatrixRepository.findOne({ where: { country: countryDB.id } });
		if (checkForCountryThisWeek) {
			await getConnection()
				.createQueryBuilder()
				.delete()
				.from(ThisWeekMatrix)
				.where('country = :country', { country: countryDB.id })
				.execute();
		}

		const maxEmployeesAllowed = async (employees) => {
			const dateToNum = (d) => {
				d = d.split('-');
				return Number(d[0] + d[1] + d[2]);
			};

			let hasRestriction;

			const data = await this.apiDataRepository.find({ where: { country } });
			const sevenDaysData = data.sort((a, b) => dateToNum(b.date) - dateToNum(a.date)).slice(0, 7);
			const avgCasesPerOneMillion = sevenDaysData
				.map((item) => item.casesPerOneMillion)
				.reduce((a, b, _, arr) => a + b / arr.length, 0);
			const avgTestsPerOneMillion = sevenDaysData
				.map((item) => item.testsPerOneMillion)
				.reduce((a, b, _, arr) => a + b / arr.length, 0);
			const ratio = (avgCasesPerOneMillion / avgTestsPerOneMillion) * 100;

			if (ratio > countryDB.upTresholdValue) {
				hasRestriction = true;
				return [Math.floor(employees * 0.5), hasRestriction];
			} else if (ratio >= countryDB.downTresholdValue && ratio <= countryDB.upTresholdValue) {
				hasRestriction = true;
				return [Math.floor(employees * 0.75), hasRestriction];
			} else {
				hasRestriction = false;
				return [employees, hasRestriction];
			}
		};

		const numberOfDesks = countryDB.numberOfDesks;
		const id = countryDB.id;

		const [maxEmployees, hasRestriction] = await maxEmployeesAllowed(numberOfDesks);

		const countryMatrix = await this.matrixRepository.find({
			where: { country: id }
		});
		if (!countryMatrix) {
			throw new BadRequestException('No country with this name');
		}

		const getAllUsersUnfiltered = await this.userRepository.find({
			where: { country: id },
			relations: [`project`, `vacation`]
		});

		const getAllUsers = getAllUsersUnfiltered
			.filter(
				(user) =>
					user.project !== null &&
					user.vacation.filter((v) => v.isDeleted !== true).every((e) => !this.isOnVacationThisWeek(e, true))
			)
			.sort((a, b) => {
				return a.thisWeekOnOffice === b.thisWeekOnOffice ? a.project.id - b.project.id : a.thisWeekOnOffice ? 1 : -1;
			})
			.map((user) => `${user.username} | ${user.project.name}`);

		const setAllUsersToFalse = await getAllUsersUnfiltered.map((u) => {
			u.thisWeekOnOffice = false;
			return u;
		});
		await this.userRepository.save(setAllUsersToFalse);

		let counter = 0;
		if (hasRestriction) {
			const workMatrix = countryMatrix.map((el, i) => {
				if (i % 2 === 0) {
					return el.value.map((col, index) => {
						if (index % 2 !== 0 && col !== '0') {
							return '2';
						} else {
							return col;
						}
					});
				} else {
					return el.value.map((col, index) => {
						if (index % 2 === 0 && col !== '0') {
							return '2';
						} else {
							return col;
						}
					});
				}
			});

			const matrixResult = await Promise.all(
				workMatrix.map((el) =>
					el.map(async (item) => {
						if (item !== '2' && item !== '0') {
							if (getAllUsers.length > 0 && counter < maxEmployees) {
								counter++;
								const userString = getAllUsers.shift().split(' ');
								const user = getAllUsersUnfiltered.find((u) => u.username === userString[0]);

								user.thisWeekOnOffice = true;
								await this.userRepository.save(user);
								return userString
									.toString()
									.split(',')
									.join(' ');
							} else {
								return item;
							}
						} else {
							return item;
						}
					})
				)
			);

			matrixResult.map(async (e, i) => {
				const newRow = {
					row: i,
					value: await Promise.all(e),
					country: countryDB
				};
				const matrix = await this.thisWeekMatrixRepository.create(newRow);

				await this.thisWeekMatrixRepository.save(matrix);
			});

			return { msg: `Schedule for ${country} added` };
		} else {
			const workMatrix = await Promise.all(
				countryMatrix.map((el) =>
					el.value.map(async (e, i) => {
						if (getAllUsers.length === 0 || e === '0') {
							return e;
						}
						if (counter < maxEmployees) {
							counter++;
							const userString = getAllUsers.shift().split(' ');
							const user = getAllUsersUnfiltered.find((u) => u.username === userString[0]);

							user.thisWeekOnOffice = true;
							await this.userRepository.save(user);
							return userString
								.toString()
								.split(',')
								.join(' ');
						}
						return e;
					})
				)
			);

			workMatrix.map(async (e, i) => {
				const newRow = {
					row: i,
					value: await Promise.all(e),
					country: countryDB
				};
				const matrix = await this.thisWeekMatrixRepository.create(newRow);

				await this.thisWeekMatrixRepository.save(matrix);
			});

			return { msg: `Schedule for ${country} added` };
		}
	}

	async setCountryScheduleNextWeek(country: string): Promise<string[][]> {
		const countryDB = await this.countryRepository.findOne({ where: { name: country } });

		const maxEmployeesAllowed = async (employees) => {
			const dateToNum = (d) => {
				d = d.split('-');
				return Number(d[0] + d[1] + d[2]);
			};

			let hasRestriction;

			const data = await this.apiDataRepository.find({ where: { country } });
			const sevenDaysData = data.sort((a, b) => dateToNum(b.date) - dateToNum(a.date)).slice(0, 7);
			const avgCasesPerOneMillion = sevenDaysData
				.map((item) => item.casesPerOneMillion)
				.reduce((a, b, _, arr) => a + b / arr.length, 0);
			const avgTestsPerOneMillion = sevenDaysData
				.map((item) => item.testsPerOneMillion)
				.reduce((a, b, _, arr) => a + b / arr.length, 0);
			const ratio = (avgCasesPerOneMillion / avgTestsPerOneMillion) * 100;

			if (ratio > countryDB.upTresholdValue) {
				hasRestriction = true;
				return [Math.floor(employees * 0.5), hasRestriction];
			} else if (ratio >= countryDB.downTresholdValue && ratio <= countryDB.upTresholdValue) {
				hasRestriction = true;
				return [Math.floor(employees * 0.75), hasRestriction];
			} else {
				hasRestriction = false;
				return [employees, hasRestriction];
			}
		};

		const numberOfDesks = countryDB.numberOfDesks;
		const id = countryDB.id;

		const [maxEmployees, hasRestriction] = await maxEmployeesAllowed(numberOfDesks);

		const countryMatrix = await this.matrixRepository.find({
			where: { country: id }
		});

		if (!countryMatrix) {
			throw new BadRequestException('No country with this name');
		}

		const getAllUsersUnfiltered = await this.userRepository.find({
			where: { country: id },
			relations: [`project`, `vacation`]
		});

		const getAllUsers = getAllUsersUnfiltered
			.filter(
				(user) =>
					user.project !== null &&
					user.vacation?.filter((v) => v.isDeleted !== true).every((e) => !this.isOnVacationThisWeek(e, false))
			)
			.sort((a, b) => {
				return a.thisWeekOnOffice === b.thisWeekOnOffice ? a.project.id - b.project.id : a.thisWeekOnOffice ? 1 : -1;
			})
			.map((user) => `${user.username} |\n ${user.project.name}`);
		let counter = 0;
		if (hasRestriction) {
			const workMatrix = countryMatrix.map((el, i) => {
				if (i % 2 === 0) {
					return el.value.map((col, index) => {
						if (index % 2 !== 0 && col !== '0') {
							return '2';
						} else {
							return col;
						}
					});
				} else {
					return el.value.map((col, index) => {
						if (index % 2 === 0 && col !== '0') {
							return '2';
						} else {
							return col;
						}
					});
				}
			});

			const matrixResult = workMatrix.map((
				el
			) =>
				el.map((item) => {
					if (item !== '2' && item !== '0') {
						if (getAllUsers.length > 0 && counter < maxEmployees) {
							counter++;
							const userString = getAllUsers.shift().split(' ');
							return userString
								.toString()
								.split(',')
								.join(' ');
						} else {
							return item;
						}
					} else {
						return item;
					}
				})
			);

			return await matrixResult;
		} else {
			const workMatrix = countryMatrix.map((
				el 
			) =>
				el.value.map((e, i) => {
					if (getAllUsers.length === 0 || e === '0') {
						return e;
					}
					if (counter < maxEmployees) {
						counter++;
						const userString = getAllUsers.shift().split(' ');
						return userString
							.toString()
							.split(',')
							.join(' ');
					}
					return e;
				})
			);

			return await workMatrix;
		}
	}

	async getCountrySchedule(country: string): Promise<string[][]> {
		const countryDB = await this.countryRepository.findOne({ where: { name: country } });
		if (!countryDB) {
			throw new BadRequestException('No country with this name');
		}
		const id = countryDB.id;
		const countryMatrix = await this.thisWeekMatrixRepository.find({ where: { country: id } });
		if (!countryMatrix) {
			throw new BadRequestException('No schedule for this country');
		}
		return countryMatrix.sort((a, b) => a.row - b.row).map((row) => row.value);
	}

	async getAllCountrySchedules(): Promise<string[]> {
		const countryMatrix = await this.thisWeekMatrixRepository.find({ relations: [`country`] });
		if (!countryMatrix) {
			throw new BadRequestException('No schedule for this country');
		}
		const countries = countryMatrix.map((c) => c.country.name);
		const unique = [...new Set(countries)];
		return unique.sort((a, b) => a.localeCompare(b));
	}

	async getInfectedPeople(country): Promise<InfectedPeople[]> {
		const infectedPeopleCountry = await this.infectedPeopleRepository.find({ where: { country, isDeleted: false } });
		return infectedPeopleCountry;
	}

	async getWeekTotalCases(country): Promise<Partial<ApiData>> {
		const totalCases = await this.apiDataRepository.find({ where: { country } });
		const sevenDaysData = totalCases
			.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime())
			.slice(0, 7);
		const avgCasesPerOneWeek = sevenDaysData
			.map((item) => {
				return {
					country: item.country,
					cases: item.cases,
					todayCases: item.todayCases,
					deaths: item.deaths,
					todayDeaths: item.todayDeaths,
					recovered: item.recovered,
					active: item.active,
					critical: item.critical
				};
			})
			.reduce(
				(a, b, _, arr) => {
					return {
						country: b.country,
						cases: a.cases + b.cases / arr.length,
						todayCases: a.todayCases + b.todayCases / arr.length,
						deaths: a.deaths + b.deaths / arr.length,
						todayDeaths: a.todayDeaths + b.todayDeaths / arr.length,
						recovered: a.recovered + b.recovered / arr.length,
						active: a.active + b.active / arr.length,
						critical: a.critical + b.critical / arr.length
					};
				},
				{
					country: '',
					cases: 0,
					todayCases: 0,
					deaths: 0,
					todayDeaths: 0,
					recovered: 0,
					active: 0,
					critical: 0
				}
			);
		return avgCasesPerOneWeek;
	}

	async addInfectedPeople(country): Promise<InfectedPeople[]> {
		const response = await fetch(`https://coronavirus-19-api.herokuapp.com/countries/${country}`);
		const data = await response.json();
		if (!data) {
			throw new BadRequestException('Problem with api fetch https://coronavirus-19-api.herokuapp.com');
		}

		const countryToCreate = {
			country: data.country,
			infectedPeople: data.cases
		};
		await this.infectedPeopleRepository.save(countryToCreate);

		return await this.infectedPeopleRepository.find({ where: { country: data.country, isDeleted: false } });
	}

	async removeSelectedRecordByID(id): Promise<ReturnMessageDTO> {
		const recordToDelete = await this.infectedPeopleRepository.findOne({ where: { id } });

		if (!recordToDelete) {
			throw new BadRequestException('No such record exists');
		}
		recordToDelete.isDeleted = true;
		await this.infectedPeopleRepository.save(recordToDelete);
		return { msg: 'Selected record removed!' };
	}

	async changeTreshold(country: string, treshold: changeTreshold): Promise<Country> {
		const findCountry = await this.countryRepository.findOne({ where: { name: country } });
		if (!findCountry) {
			throw new BadRequestException('Country not found!');
		}
		if (treshold.downTreshold) {
			findCountry.downTresholdValue = treshold.downTreshold;
		}
		if (treshold.upTreshold) {
			findCountry.upTresholdValue = treshold.upTreshold;
		}
		await this.countryRepository.save(findCountry);
		return findCountry;
	}
}
