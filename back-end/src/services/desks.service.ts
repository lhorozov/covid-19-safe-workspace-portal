import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformService } from './transform.service';
import fetch from 'node-fetch';
import { ApiData } from 'src/models/api-data.entity';
import { Cron } from '@nestjs/schedule';
import { RequestCountryDTO } from 'src/dtos/country.dto';
import { DesksReportDTO } from 'src/dtos/desks-report.dto';
import { ThisWeekMatrix } from 'src/models/this-week-matrix.entity';
import { Country } from 'src/models/country.entity';

@Injectable()
export class DesksService {
	constructor(
		@InjectRepository(ApiData) private readonly apiDataRepository: Repository<ApiData>,
		@InjectRepository(ThisWeekMatrix) private readonly thisWeekMatrixRepository: Repository<ThisWeekMatrix>,
		@InjectRepository(Country) private readonly countryRepository: Repository<Country>,
	) {}

	@Cron('0 30 11 * * 1-7') // Everyday at 11:30h
	async handleApiFetch() {
		const response = await fetch(`https://coronavirus-19-api.herokuapp.com/countries`);
		const data = await response.json();
		const dataToImport = await this.apiDataRepository.create(data);
		await this.apiDataRepository.save(dataToImport);
	}

	async getApiDataByCountry(country: string): Promise<RequestCountryDTO[]> {
		const countryResult = await this.apiDataRepository.find({
			where: { country }
		});
		if (!countryResult) {
			throw new BadRequestException('No country with this name');
		}
		return countryResult;
	}

	async getApiData(): Promise<RequestCountryDTO[]> {
		const countryResult = await this.apiDataRepository.find();
		if (!countryResult) {
			throw new BadRequestException('No country with this name');
		}
		return countryResult;
	}

	async getDesksReport(): Promise<DesksReportDTO[]> {
		const thisWeekMatrix = await this.thisWeekMatrixRepository.find({ relations: ['country'] });
		const thisWeekCountries = thisWeekMatrix.map(el => el.country.name);

		const countries = await (await this.countryRepository.find())
			.filter(c => thisWeekCountries.includes(c.name))
			.sort((a, b) => a.name.localeCompare(b.name));
		
		
		const flatWeekMatrix = thisWeekMatrix
			.sort((a, b) => a.country.name.localeCompare(b.country.name) || a.row - b.row)
			.map(row => row.value)
			.reduce((acc, val) => acc.concat(val), []);

		const separatedWeekMatrix = countries.map(country => {
			const countryArray = flatWeekMatrix.splice(0, country.numberOfDesks)

			const forbiddenDesks = countryArray.filter(e => e === '2').length;
			const availableDesks = countryArray.filter(e => e === '1').length;
			const occupiedDesks = country.numberOfDesks - forbiddenDesks - availableDesks;
			return {
				country: country.name,
				numberOfDesks: country.numberOfDesks,
				occupiedDesks,
				forbiddenDesks,
				availableDesks,
			}
		})
			
		return separatedWeekMatrix;
	}
}
