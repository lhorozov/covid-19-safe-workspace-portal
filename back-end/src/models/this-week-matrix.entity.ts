import { PrimaryGeneratedColumn, Column, Entity, ManyToOne } from 'typeorm';
import { Country } from './country.entity';

@Entity('thisweekmatrix')
export class ThisWeekMatrix {
	@PrimaryGeneratedColumn('increment')
	id: number;

	@Column()
	row: number;

	@Column('simple-array')
	value: string[];

	@ManyToOne(
		() => Country,
		(c) => c.desks
	)
	country: Country;
}