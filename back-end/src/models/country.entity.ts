import { PrimaryGeneratedColumn, Column, Entity, OneToMany } from 'typeorm';
import { User } from './users.entity';
import { Project } from './project.entity';
import { Desk } from './desks.entity';
import { Matrix } from './matrix.entity';


@Entity('countries')
export class Country {

  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('nvarchar', { length: 50 })
  name: string;

  @Column()
  numberOfDesks: number;

  @Column()
  numberOfDesksPerRow: number;

  @Column()
  columnSpace: number;

  @Column({default: 3, type: "double"})
  setDistanceX: number;

  @Column({default: 2, type: "double"})
  setDistanceY: number;

  @Column({default: 5})
  downTresholdValue: number;

  @Column({default: 10})
  upTresholdValue: number;

  @OneToMany(
    () => User,
    u => u.country,
  )
  users: User[];


  @OneToMany(
    () => Project,
    p => p.country,
  )
  projects: Project[];

  @OneToMany(
    () => Desk,
    p => p.country,
  )
  desks: Desk[];

  @OneToMany(
    () => Matrix,
    p => p.country,
  )
  rows: Matrix[];

}
