export enum DeskStatus {
    Occupied = 1,
    Available = 2,
    Forbidden = 3,
  }