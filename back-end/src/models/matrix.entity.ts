import { PrimaryGeneratedColumn, Column, Entity, ManyToOne } from 'typeorm';
import { Country } from './country.entity';

@Entity('matrix')
export class Matrix {
	@PrimaryGeneratedColumn('increment')
	id: number;

	@Column()
	row: number;

	@Column('simple-array')
	value: string[];

	@ManyToOne(
		() => Country,
		(c) => c.desks
	)
	country: Country;
}
