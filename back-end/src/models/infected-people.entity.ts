import { PrimaryGeneratedColumn, Column, Entity } from 'typeorm';

@Entity('infected-people')
export class InfectedPeople {
	@PrimaryGeneratedColumn('increment')
	id: number;

  @Column({ nullable: false, type: 'datetime', default: () => "CURRENT_TIMESTAMP" })
  date: string;

  @Column({nullable: true})
  infectedPeople: number;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;
  
  @Column('nvarchar')
  country: string;

}