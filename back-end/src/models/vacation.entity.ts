import { PrimaryGeneratedColumn, Column, Entity, ManyToOne } from 'typeorm';
import { User } from './users.entity';

@Entity('vacation')
export class Vacation {
	@PrimaryGeneratedColumn('increment')
	id: number;

	@Column({ type: 'date' })
	startDate: Date;

	@Column({ type: 'date' })
	endDate: Date;

	@Column({ type: 'boolean', default: false })
	isDeleted: boolean;

	@ManyToOne(
		() => User,
		(c) => c.vacation
	)
	user: User;
}
