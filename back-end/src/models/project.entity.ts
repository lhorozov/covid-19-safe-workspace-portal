import { PrimaryGeneratedColumn, Column, Entity, OneToMany, ManyToOne } from 'typeorm';
import { Country } from './country.entity';
import { User } from './users.entity';

@Entity('projects')
export class Project {
	@PrimaryGeneratedColumn('increment')
	id: number;

	@Column('nvarchar', { length: 50 })
	name: string;

	@Column('nvarchar', { length: 100 })
	description: string;

	@ManyToOne(
		() => Country,
		(c) => c.projects
	)
	country: Country;

	@OneToMany(
		() => User,
		(p) => p.project
	)
	users: User[];
}
