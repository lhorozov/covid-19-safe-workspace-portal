import { PrimaryGeneratedColumn, Column, Entity, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { Country } from './country.entity';
import { DeskStatus } from './enums/desk-status';
import { User } from './users.entity';

@Entity('desks')
export class Desk {
	@PrimaryGeneratedColumn('increment')
	id: number;

	@Column({
		type: 'enum',
		enum: DeskStatus,
		default: DeskStatus.Available
	})
	deskStatus: DeskStatus;

	@OneToOne((type) => User)
	@JoinColumn()
	user: User;

	@ManyToOne(
		() => Country,
		(c) => c.desks
	)
	country: Country;
}
