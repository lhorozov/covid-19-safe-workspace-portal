import { PrimaryGeneratedColumn, Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { UserRole } from './enums/user-role';
import { Country } from './country.entity';
import { Project } from './project.entity';
import { Vacation } from './vacation.entity';

@Entity('users')
export class User {
	@PrimaryGeneratedColumn('increment')
	id: number;

	@Column('nvarchar', { length: 50 })
	username: string;

	@Column('nvarchar', { length: 50 })
	firstName: string;

	@Column('nvarchar', { length: 50 })
	lastName: string;

	@Column('nvarchar')
	password: string;

	@Column('nvarchar')
	email: string;

	@Column({ type: 'boolean', default: false })
	thisWeekOnOffice: boolean;

	@Column({ type: 'boolean', default: false })
	isDeleted: boolean;

	@Column({
		type: 'enum',
		enum: UserRole,
		default: UserRole.Basic
	})
	role: UserRole;


	@ManyToOne(
		() => Country,
		(c) => c.users
	)
	country: Country;

	@ManyToOne(
		() => Project,
		(c) => c.users
	)
	project: Project;

	@OneToMany(
		() => Vacation,
		c => c.user,
	  )
	  vacation: Vacation[];

}
