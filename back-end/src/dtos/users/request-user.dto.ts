import { IsString, Length, IsEmail, Allow } from 'class-validator';

export class requestUserDTO {
  @IsString()
  @Length(5, 50)
  username: string;

  @IsString()
  @Length(2, 50)
  firstName: string;

  @IsString()
  @Length(2, 50)
  lastName: string;

  @IsString()
  @Length(5, 50)
  password: string;

  @IsEmail()
  email: string;

}