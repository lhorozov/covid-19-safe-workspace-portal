export class User {
  id: number;
  username: string;
  password: string;
  isDeleted: boolean;
  borrowed: number[];
  read: number[];
}