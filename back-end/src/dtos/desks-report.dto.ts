export class DesksReportDTO {
	country: string;
	numberOfDesks: number;
	occupiedDesks: number;
  forbiddenDesks: number;
  availableDesks: number;
}