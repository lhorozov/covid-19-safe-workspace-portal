export class DeskPlanningDTO {
	numberOfDesks: number;
	numberOfDesksPerRow: number;
	columnSpace: number;
	downTresholdValue: number;
	upTresholdValue: number;
	setDistanceX: number;
	setDistanceY: number;
}
