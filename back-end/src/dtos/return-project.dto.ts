import { Country } from "src/models/country.entity";

export class ReturnProjectDTO {
    name: string;
    description: string;
  }