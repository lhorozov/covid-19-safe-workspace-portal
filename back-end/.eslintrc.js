// module.exports = {
// 	env: {
// 		browser: true,
// 		es2020: true
// 	},
// 	extends: ['google', 'prettier'],
// 	parser: '@typescript-eslint/parser',
// 	parserOptions: {
// 		ecmaVersion: 12,
// 		sourceType: 'module'
// 	},
// 	plugins: ['@typescript-eslint'],
// 	rules: {
// 		arrowParens: 'always',
// 		bracketSpacing: true,
// 		embeddedLanguageFormatting: 'auto',
// 		htmlWhitespaceSensitivity: 'css',
// 		insertPragma: false,
// 		jsxBracketSameLine: false,
// 		jsxSingleQuote: true,
// 		printWidth: 80,
// 		proseWrap: 'preserve',
// 		quoteProps: 'as-needed',
// 		requirePragma: false,
// 		semi: true,
// 		singleQuote: true,
// 		tabWidth: 4,
// 		trailingComma: 'none',
// 		useTabs: true,
// 		vueIndentScriptAndStyle: false
// 	}
// };
