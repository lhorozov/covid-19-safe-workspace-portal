const { createExpressionWithTypeArguments } = require('typescript');

module.exports = {
	singleQuote: true,
	arrowParens: 'always',
	bracketSpacing: true,
	embeddedLanguageFormatting: 'auto',
	htmlWhitespaceSensitivity: 'css',
	insertPragma: false,
	jsxBracketSameLine: false,
	jsxSingleQuote: true,
	printWidth: 120,
	proseWrap: 'preserve',
	quoteProps: 'as-needed',
	requirePragma: false,
	semi: true,
	tabWidth: 2,
	trailingComma: 'none',
	useTabs: true,
	vueIndentScriptAndStyle: false
};
